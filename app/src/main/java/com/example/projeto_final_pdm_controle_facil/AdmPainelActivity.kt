package com.example.projeto_final_pdm_controle_facil

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import com.android.volley.Request
import com.example.projeto_final_pdm_controle_facil.api_reference.API
import com.example.projeto_final_pdm_controle_facil.api_reference.Routes
import com.example.projeto_final_pdm_controle_facil.global_data_control.UserControl
import com.example.projeto_final_pdm_controle_facil.ui.activities.financies.MainFinanciesActivity
import com.example.projeto_final_pdm_controle_facil.global_data_control.CompanyControl
import com.example.projeto_final_pdm_controle_facil.global_data_control.ProductControl
import com.example.projeto_final_pdm_controle_facil.helper.GsonHelper
import com.example.projeto_final_pdm_controle_facil.http.JSONObjectRequestConcrete
import com.example.projeto_final_pdm_controle_facil.model.Company
import com.example.projeto_final_pdm_controle_facil.model.Product
import com.example.projeto_final_pdm_controle_facil.model.User
import com.example.projeto_final_pdm_controle_facil.ui.activities.products.InsertProductActivity
import com.example.projeto_final_pdm_controle_facil.ui.activities.products.MainProductsActivity
import com.example.projeto_final_pdm_controle_facil.ui.activities.provider.MainProvidersActivity
import com.example.projeto_final_pdm_controle_facil.ui.activities.reports.MainReportsActivity
import com.example.projeto_final_pdm_controle_facil.ui.activities.user.UpdateUserActivity
import org.json.JSONObject
import kotlin.system.exitProcess

class AdmPainelActivity : AppCompatActivity() {

    //==============================================================================================
    //                                        VIEWS
    //==============================================================================================
    private lateinit var userName        : TextView
    private lateinit var companyName     : TextView
    private lateinit var backButton      : ImageView
    private lateinit var logout          : ImageView
    private lateinit var progressBarMain : ProgressBar
    private lateinit var resultLauncherUpdateUser  : ActivityResultLauncher<Intent>
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adm_painel)

        //==========================================================================================
        this.userName        = findViewById(R.id.userName)
        this.companyName     = findViewById(R.id.companyName)
        this.backButton      = findViewById(R.id.backButton)
        this.logout          = findViewById(R.id.logout)
        this.progressBarMain = findViewById(R.id.progressBarMain)
        //==========================================================================================


        JSONObjectRequestConcrete(
            this,
            Request.Method.POST,
            Routes.USER_NAME.path,
            API.CURRENT.location,
            null,
            { response ->

                val user = GsonHelper().deserialize<User>(response.toString()) as User
                UserControl.addUser(user)
                this.userName.text = UserControl.getUserAuth().name

                JSONObjectRequestConcrete(
                    baseContext,
                    Request.Method.GET,
                    Routes.USER_COMPANY.path + UserControl.getUserAuth().id_empresa,
                    API.CURRENT.location,
                    JSONObject(),
                    { response ->
                        val company = GsonHelper().deserialize<Company>(response.toString()) as Company
                        CompanyControl.addCompany(company)
                        this.companyName.text = CompanyControl.getUserCompany().nome_empresa

                    },
                    {
                        Toast.makeText(baseContext, "${it.networkResponse}", Toast.LENGTH_SHORT).show()
                    }).performRequestWithAuthentication()
            },
            {
                Toast.makeText(baseContext, "${it.networkResponse}", Toast.LENGTH_SHORT).show()
            }
        ).performRequestWithAuthentication()



        buildResultLauncherUpdateUser()
    }


    //Se clicarmos em produtos
    fun onClickProduct(v : View){
        if(UserControl.userIsAuthenticated()){
            val requestIntent = Intent(this, MainProductsActivity::class.java)
            startActivity(requestIntent)
        } else {
            Toast.makeText(this, R.string.user_is_not_authorized, Toast.LENGTH_SHORT).show()
        }
    }

    //Se clicarmos em fornecedores
    fun onClickProvider(v : View){
        if(UserControl.userIsAuthenticated()) {
            val requestIntent = Intent(this, MainProvidersActivity::class.java)
            startActivity(requestIntent)
        } else {
            Toast.makeText(this, R.string.user_is_not_authorized, Toast.LENGTH_SHORT).show()
        }
    }

    //Se clicarmos em financies
    fun onClickFinancies(v : View){
        if(UserControl.userIsAuthenticated()) {
            val requestIntent = Intent(this, MainFinanciesActivity::class.java)
            startActivity(requestIntent)
        } else {
            Toast.makeText(this, R.string.user_is_not_authorized, Toast.LENGTH_SHORT).show()
        }
    }

    //Se clicarmos em relatórios
    fun onClickReport(v : View){
        if(UserControl.userIsAuthenticated()) {
            val requestIntent = Intent(this, MainReportsActivity::class.java)
            startActivity(requestIntent)
        } else {
            Toast.makeText(this, R.string.user_is_not_authorized, Toast.LENGTH_SHORT).show()
        }
    }


    //==============================================================================================
    //                                  USER INFORMATION
    //==============================================================================================
    fun onClickUserInformation(v : View){
        if(UserControl.userIsAuthenticated()) {
            val requestUser = Intent(this, UpdateUserActivity::class.java)
            this.resultLauncherUpdateUser.launch(requestUser)
        } else {
            Toast.makeText(this, R.string.user_is_not_authorized, Toast.LENGTH_SHORT).show()
        }
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================



    //==============================================================================================
    //                                  UPDATE USER
    //==============================================================================================
    private fun buildResultLauncherUpdateUser(){
        val contract = ActivityResultContracts.StartActivityForResult()
        this.resultLauncherUpdateUser = registerForActivityResult(contract){
                result ->
            if(result.resultCode == RESULT_OK && result.data != null){
                val resp = result.data!!.getIntExtra("USUARIO_RESPOSTA", -1)
                if(resp == 1) {
                    this.userName.text = UserControl.getUserAuth().name.toString()
                    Toast.makeText(this, R.string.toast_user_update_success, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    //==============================================================================================
    //                                        BACK BUTTON
    //==============================================================================================
    fun onClickBackButton(v : View){
        super.onBackPressed()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    //==============================================================================================
    //                                        LOGOUT BUTTON
    //==============================================================================================
    fun onLogoutButton(v : View){
        this.progressBarMain.visibility = View.VISIBLE
        JSONObjectRequestConcrete(
            this,
            Request.Method.POST,
            Routes.USER_LOGOUT.path,
            API.CURRENT.location,
            null,
            { _ ->
                this.progressBarMain.visibility = View.GONE
                moveTaskToBack(true);
                exitProcess(-1)
            },

            { _ ->
                this.progressBarMain.visibility = View.GONE
                Toast.makeText(this, R.string.toast_error_logout, Toast.LENGTH_SHORT).show()
            }).performRequestWithAuthentication()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================

}