package com.example.projeto_final_pdm_controle_facil.global_data_control

import com.example.projeto_final_pdm_controle_facil.model.User

object UserControl {

    //Object User
    lateinit var user: User

    //add a user
    fun addUser(user : User){
        this.user = user
    }

    //get the authenticated user
    fun getUserAuth() : User = user

    fun userIsAuthenticated() : Boolean {
        return this::user.isInitialized
    }
}