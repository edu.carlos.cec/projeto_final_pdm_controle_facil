package com.example.projeto_final_pdm_controle_facil.global_data_control

import android.content.Context
import com.example.projeto_final_pdm_controle_facil.model.Product

object ProductControl {

    private lateinit var products : ArrayList<Product>

    private fun initDAO() {
        if(!::products.isInitialized){
            this.products = ArrayList<Product>()
        }
    }

    //Add all products
    fun addAllProducts(products :ArrayList<Product>){
        initDAO()
        this.products = products
    }

    //create
    fun add(product : Product) : Int {
        initDAO()
        products.add(0, product)
        return 0
    }


    //read
    fun getAllProducts() : ArrayList<Product>{
        initDAO()
        return products
    }


    //update
    fun update(product : Product) : Int{
        initDAO()
        val pos = this.products.indexOf(product)
        val prod = this.products.get(pos)
        prod.nome_produto = product.nome_produto
        prod.id_fornecedor = product.id_fornecedor
        prod.preco = product.preco
        prod.qtd_disponivel = product.qtd_disponivel
        prod.status = product.status
        return pos
    }


    //Delete
    fun delete(product : Product) : Int{
        initDAO()
        val pos = this.products.indexOf(product)
        this.products.removeAt(pos)
        return pos
    }



















    //Object User
    lateinit var product: Product

    //add a user
    fun addProduct(product : Product){
        this.product = product
    }

    //get the authenticated user
    fun getCurrentProduct() : Product = product


}