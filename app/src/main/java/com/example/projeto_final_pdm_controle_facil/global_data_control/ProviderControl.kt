package com.example.projeto_final_pdm_controle_facil.global_data_control

import com.example.projeto_final_pdm_controle_facil.model.Product
import com.example.projeto_final_pdm_controle_facil.model.Provider

object ProviderControl {

    lateinit var providers: ArrayList<Provider>

    fun initDAO(){
        if(!::providers.isInitialized){
            this.providers = ArrayList<Provider>()
        }
    }

    //ADICIONAR
    fun addAllProviders(providers : ArrayList<Provider>){
        initDAO()
        this.providers = providers
    }

    //CREATE
    fun add(provider : Provider) : Int {
        initDAO()
        providers.add(0, provider)
        return 0
    }


    //READ
    fun getAllProviders() : ArrayList<Provider>{
        initDAO()
        return providers
    }

    //UPDATE
    fun update(provider : Provider) : Int{
        initDAO()
        val pos  = this.providers.indexOf(provider)
        val prov = this.providers.get(pos)
        prov.nome_fornecedor     = provider.nome_fornecedor
        prov.endereco_fornecedor = provider.endereco_fornecedor
        return pos
    }

    //Delete
    fun delete(provider : Provider) : Int {
        initDAO()
        val pos = this.providers.indexOf(provider)
        this.providers.removeAt(pos)
        return pos
    }
}