package com.example.projeto_final_pdm_controle_facil.global_data_control

import com.example.projeto_final_pdm_controle_facil.model.Company

object CompanyControl {

    //Company
    lateinit var company : Company

    //Add the Company to the Singleton
    fun addCompany(company : Company){
        this.company = company
    }

    //Add the Company to the Singleton
    fun getUserCompany() : Company{
        return company
    }
}