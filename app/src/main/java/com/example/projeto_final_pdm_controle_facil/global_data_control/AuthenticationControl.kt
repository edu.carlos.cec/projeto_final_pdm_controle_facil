package com.example.projeto_final_pdm_controle_facil.global_data_control

import com.example.projeto_final_pdm_controle_facil.model.response.ResponseAuthentication

object AuthenticationControl {

    lateinit var responseAuthentication: ResponseAuthentication

    //Adicionando a resposta de autenticacao
    fun addResponseAuthentication(responseAuthentication: ResponseAuthentication) {
        this.responseAuthentication = responseAuthentication
    }

    fun getAccessToken() : String{
        return this.responseAuthentication.access_token
    }

}