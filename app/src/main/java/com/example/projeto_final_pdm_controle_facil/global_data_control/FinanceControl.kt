package com.example.projeto_final_pdm_controle_facil.global_data_control

import com.example.projeto_final_pdm_controle_facil.model.SellHistory

object FinanceControl {

    private lateinit var finances : ArrayList<SellHistory>

    private fun initDAO() {
        if(!::finances.isInitialized){
            this.finances = ArrayList<SellHistory>()
        }
    }


    fun addAllSells(finances :ArrayList<SellHistory>){
        initDAO()
        this.finances = finances
    }

    fun getAllSells(): ArrayList<SellHistory> {
        initDAO()
        return this.finances
    }
}