package com.example.projeto_final_pdm_controle_facil

import android.content.Intent
import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.*
import com.android.volley.Request
import com.example.projeto_final_pdm_controle_facil.api_reference.API
import com.example.projeto_final_pdm_controle_facil.api_reference.Routes
import com.example.projeto_final_pdm_controle_facil.global_data_control.AuthenticationControl
import com.example.projeto_final_pdm_controle_facil.global_data_control.CompanyControl
import com.example.projeto_final_pdm_controle_facil.helper.GsonHelper
import com.example.projeto_final_pdm_controle_facil.http.JSONObjectRequestConcrete
import com.example.projeto_final_pdm_controle_facil.model.User
import com.example.projeto_final_pdm_controle_facil.model.response.ResponseAuthentication
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    //==============================================================================================
    //                                        VIEWS
    //==============================================================================================
    private lateinit var emailUser          : TextView
    private lateinit var passwordUser       : EditText
    private lateinit var btnEntrar          : Button
    private lateinit var passwordVisibility : CheckBox
    private lateinit var progressBar        : ProgressBar
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //==========================================================================================
        this.emailUser          = findViewById(R.id.emailUser)
        this.passwordUser       = findViewById(R.id.passwordUser)
        this.btnEntrar          = findViewById(R.id.btnEntrar)
        this.passwordVisibility = findViewById(R.id.passwordVisibility)
        this.progressBar        = findViewById(R.id.progressBar)
        //==========================================================================================

        this.progressBar.visibility = View.GONE
    }


    //==============================================================================================
    //                                       PASSWORD VISIBILITY
    //==============================================================================================
    fun onClickBtnEntrar(v : View){

        val user = User(
            id_usuario        = null,
            id_empresa        = null,
            name              = null,
            email             = this.emailUser.text.trim().toString(),
            password          = this.passwordUser.text.trim().toString(),
            email_verified_at = null,
            created_at        = null,
            updated_at        = null
        )

        if( !user.validate() ){
            Toast.makeText(this, R.string.toast_required_creadentials, Toast.LENGTH_SHORT).show()
        } else {

            //JSON OBJECT
            val credencials : JSONObject? = JSONObject()
            credencials?.put("email",    this.emailUser.text.toString())
            credencials?.put("password", this.passwordUser.text.toString())

            this.progressBar.visibility = View.VISIBLE

            JSONObjectRequestConcrete(this, Request.Method.POST, Routes.USER_AUTHENTICATION.path, API.CURRENT.location, credencials,
                {
                    response ->
                        val reponseAuthentication =
                            GsonHelper().deserialize<ResponseAuthentication>(response.toString())
                        AuthenticationControl.addResponseAuthentication(reponseAuthentication as ResponseAuthentication)
                        this.progressBar.visibility = View.GONE
                        Toast.makeText(baseContext, R.string.toast_user_logged, Toast.LENGTH_LONG).show()
                        val request = Intent(this, AdmPainelActivity::class.java)
                        startActivity(request)
                },
                {
                     _ ->
                        this.progressBar.visibility = View.GONE
                        Toast.makeText(this, R.string.toast_required_creadentials, Toast.LENGTH_SHORT).show()
                }).performRequestAuthentication()

        }
    }

    //==============================================================================================
    //                                       PASSWORD VISIBILITY
    //==============================================================================================
    fun onClickPasswordVisibility(v : View){
        v as CheckBox
        if(v.isChecked){
            this.passwordUser.inputType = InputType.TYPE_CLASS_TEXT
            this.passwordVisibility.setBackgroundResource(R.drawable.ic_baseline_visibility)
        } else {
            this.passwordUser.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
            this.passwordVisibility.setBackgroundResource(R.drawable.ic_baseline_visibility_off)
        }
        this.passwordUser.setSelection(this.passwordUser.length())
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================

}