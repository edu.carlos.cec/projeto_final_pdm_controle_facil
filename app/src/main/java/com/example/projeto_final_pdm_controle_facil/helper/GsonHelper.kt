package com.example.projeto_final_pdm_controle_facil.helper

import com.example.projeto_final_pdm_controle_facil.model.Model
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class GsonHelper
{

    //Serialização dos dados =======================================================================
    //Recebe um objeto do Kotlin e Retorna o JSON em forma de String
    fun serialize(obj : Any): String {
        return Gson().toJson(obj)
    }
    //==============================================================================================


    //Deserialização dos dados =====================================================================
    inline fun <reified T : Any> deserialize(json: String): Any = Gson().fromJson(json, T::class.java) as Any
    //==============================================================================================


    //Deserialização de Array de Dados =============================================================
    inline fun <reified T : Model> deserializeArray(json : String): ArrayList<T> {
        val typeToken = object : TypeToken<ArrayList<T>>() {}.type
        return Gson().fromJson<ArrayList<T>>(json, typeToken)
    }
    //==============================================================================================
}