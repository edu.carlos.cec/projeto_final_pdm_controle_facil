package com.example.projeto_final_pdm_controle_facil.http

import android.content.Context
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.projeto_final_pdm_controle_facil.global_data_control.AuthenticationControl
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONObject
import java.util.HashMap

class JSONArrayRequestConcrete(
    context            : Context,
    method             : Int,
    resource           : String,
    baseUrl            : String,
    jsonRequest        : JSONArray?,
    responseListener   : Response.Listener<JSONArray?>,
    errorListener      : Response.ErrorListener
) : RequestFactory <JSONArray?>(
    context,
    method,
    resource,
    baseUrl,
    jsonRequest,
    responseListener,
    errorListener
) {
    override fun performRequestAuthentication() {
        val queue = Volley.newRequestQueue(context)
        val jsonArrayRequest = JsonArrayRequest(
            this.method,
            this.baseUrl + this.resource,
            this.jsonRequest,
            this.responseListener,
            this.errorListener)
        queue.add(jsonArrayRequest)
    }

    override fun performRequestWithAuthentication() {
        val queue = Volley.newRequestQueue(context)
        val JsonArrayRequest = object : JsonArrayRequest(
            this.method,
            this.baseUrl + this.resource,
            this.jsonRequest,
            this.responseListener,
            this.errorListener){
            @Throws(AuthFailureError::class)
            override fun getHeaders(): MutableMap<String, String> {
                var params: MutableMap<String, String> = super.getHeaders()
                if (params.isEmpty()) {
                    params = HashMap()
                }
                params["Content-Type"] = "application/json"
                params["Authorization"] = "Bearer ${AuthenticationControl.getAccessToken()}"
                return params
            }
        }
        queue.add(JsonArrayRequest)
    }


}