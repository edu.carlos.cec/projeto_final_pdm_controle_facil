package com.example.projeto_final_pdm_controle_facil.http

import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.projeto_final_pdm_controle_facil.AdmPainelActivity
import com.example.projeto_final_pdm_controle_facil.api_reference.API
import com.example.projeto_final_pdm_controle_facil.global_data_control.AuthenticationControl
import com.example.projeto_final_pdm_controle_facil.model.response.ResponseAuthentication
import com.google.gson.Gson
import okhttp3.Request
import org.json.JSONObject
import java.util.HashMap

class JSONObjectRequestConcrete(
    context            : Context,
    method             : Int,
    resource           : String = "",
    baseUrl            : String = API.CURRENT.location,
    jsonRequest        : JSONObject?,
    responseListener   : Response.Listener<JSONObject?>,
    errorListener      : Response.ErrorListener
) : RequestFactory <JSONObject?> (
    context,
    method,
    resource,
    baseUrl,
    jsonRequest,
    responseListener,
    errorListener
) {
    override fun performRequestAuthentication() {
        val queue = Volley.newRequestQueue(context)
        val jsonObjectRequest = JsonObjectRequest(
            this.method,
            this.baseUrl + this.resource,
            this.jsonRequest,
            this.responseListener,
            this.errorListener)
        queue.add(jsonObjectRequest)
    }

    override fun performRequestWithAuthentication() {
        val queue = Volley.newRequestQueue(context)
        val jsonObjectRequest = object : JsonObjectRequest(
            this.method,
            this.baseUrl + this.resource,
            this.jsonRequest,
            this.responseListener,
            this.errorListener){
            @Throws(AuthFailureError::class)
            override fun getHeaders(): MutableMap<String, String> {
                var params: MutableMap<String, String> = super.getHeaders()
                if (params.isEmpty()) {
                    params = HashMap()
                }
                params["Content-Type"] = "application/json"
                params["Authorization"] = "Bearer ${AuthenticationControl.getAccessToken()}"
                return params
            }
        }
        queue.add(jsonObjectRequest)
    }
}