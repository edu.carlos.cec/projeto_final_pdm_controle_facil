package com.example.projeto_final_pdm_controle_facil.http

import android.content.Context
import com.android.volley.Response
import com.example.projeto_final_pdm_controle_facil.api_reference.API
import org.json.JSONObject

//Função feita para criar tipos de requisição
abstract class RequestFactory<TYPE_RESPONSE>(
    val context            : Context,
    val method             : Int,
    val resource           : String = "",
    val baseUrl            : String = API.CURRENT.location,
    val jsonRequest        : TYPE_RESPONSE,
    val responseListener   : Response.Listener<TYPE_RESPONSE>,
    val errorListener      : Response.ErrorListener
) {
    abstract fun performRequestAuthentication()
    abstract fun performRequestWithAuthentication()
}