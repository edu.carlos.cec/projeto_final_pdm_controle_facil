package com.example.projeto_final_pdm_controle_facil.model

import android.graphics.pdf.PdfDocument
import android.os.Parcel
import android.os.Parcelable

class Provider (
        val id_fornecedor       : Int?,
        var nome_fornecedor     : String?,
        var endereco_fornecedor : String?,
        var created_at          : String?,
        var updated_at          : String?,
) : Model(), Parcelable
{

        //==========================================================================================
        //                                      VALIDATOR
        //==========================================================================================
        override fun validate() : Boolean{
                return  (
                           this.nome_fornecedor!!.isNotEmpty() &&
                           this.nome_fornecedor!!.length <= 50
                        ) &&
                        (
                           this.endereco_fornecedor!!.isNotEmpty() &&
                           this.endereco_fornecedor!!.length <= 50
                        )
        }
        //==========================================================================================
        //==========================================================================================
        //==========================================================================================

        override fun toString(): String {
                return "Fornecedor(id_fornecedor=$id_fornecedor, nome_fornecedor='$nome_fornecedor', endereco_fornecedor='$endereco_fornecedor', created_at='$created_at', updated_at='$updated_at')"
        }

        override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (javaClass != other?.javaClass) return false

                other as Provider

                if (id_fornecedor != other.id_fornecedor) return false

                return true
        }

        override fun hashCode(): Int {
                return id_fornecedor ?: 0
        }

        //==========================================================================================
        //                                      PARCELABLE
        //==========================================================================================
        constructor(parcel: Parcel) : this(
                parcel.readValue(Int::class.java.classLoader) as? Int,
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString()
        ) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeValue(id_fornecedor)
                parcel.writeString(nome_fornecedor)
                parcel.writeString(endereco_fornecedor)
                parcel.writeString(created_at)
                parcel.writeString(updated_at)
        }

        override fun describeContents(): Int {
                return 0
        }


        companion object CREATOR : Parcelable.Creator<Provider> {
                override fun createFromParcel(parcel: Parcel): Provider {
                        return Provider(parcel)
                }

                override fun newArray(size: Int): Array<Provider?> {
                        return arrayOfNulls(size)
                }
        }
        //==========================================================================================
        //==========================================================================================
        //==========================================================================================
}