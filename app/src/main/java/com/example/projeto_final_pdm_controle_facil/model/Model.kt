package com.example.projeto_final_pdm_controle_facil.model

import com.example.projeto_final_pdm_controle_facil.global_data_control.AuthenticationControl
import com.example.projeto_final_pdm_controle_facil.model.response.ResponseAuthentication
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

abstract class Model() {

    //Validação dos dados ==========================================================================
    abstract fun validate() : Boolean
    //==============================================================================================

}