package com.example.projeto_final_pdm_controle_facil.model.response

class ResponseAuthentication (
    var access_token : String,
    var token_type   : String,
    var expires_in   : String,
) {

    override fun toString(): String {
        return "ResponseAuthentication(access_token='$access_token', token_type='$token_type', expires_in='$expires_in')"
    }
}