package com.example.projeto_final_pdm_controle_facil.model

class Company(
    val id_empresa   : Int,
    val nome_empresa : String,
    val cnpj         : String,
    val created_at   : String,
    val updated_at   : String
) {

    override fun toString(): String {
        return "Empresa( empresa=$id_empresa, nomeEmpresa='$nome_empresa', cnpj='$cnpj', created_at='$created_at', updated_at='$updated_at' )"
    }
}