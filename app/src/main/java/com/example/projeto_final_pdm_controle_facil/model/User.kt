package com.example.projeto_final_pdm_controle_facil.model

import android.annotation.SuppressLint
import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import android.widget.Toast
import com.example.projeto_final_pdm_controle_facil.R

class User (

    val id_usuario        : Int?    = null,
    val id_empresa        : Int?    = null,
    var name              : String? = null,
    var email             : String? = null,
    var password          : String? = null,
    val email_verified_at : Int?    = null,
    val created_at        : String? = null,
    val updated_at        : String? = null

    ) : Model() , Parcelable{

    override fun validate(): Boolean {
        val a = (
                        this.email.toString().isNotEmpty() &&
                        this.email.toString().length <= 50
                ) &&
                (
                        this.password.toString().isNotEmpty() &&
                        this.password.toString().length <= 50
                )

        Log.d("PRODUCT", this.toString())
        Log.d("PRODUCT", a.toString())

        return (
                    this.email.toString().isNotEmpty() &&
                    this.email.toString().length <= 50
            ) &&
            (
                    this.password.toString().isNotEmpty() &&
                    this.password.toString().length <= 50
            )
    }

    override fun toString(): String {
        return "Usuario(id_usuario=$id_usuario, id_empresa=$id_empresa, name='$name', email='$email', email_verified_at=$email_verified_at, created_at='$created_at', updated_at='$updated_at')"
    }


    //==============================================================================================
    //                                       PARCELABLE
    //==============================================================================================

    constructor(parcel: Parcel) : this(
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id_usuario)
        parcel.writeValue(id_empresa)
        parcel.writeString(name)
        parcel.writeString(email)
        parcel.writeString(password)
        parcel.writeValue(email_verified_at)
        parcel.writeString(created_at)
        parcel.writeString(updated_at)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }

    //==============================================================================================
    //==============================================================================================
    //==============================================================================================
}