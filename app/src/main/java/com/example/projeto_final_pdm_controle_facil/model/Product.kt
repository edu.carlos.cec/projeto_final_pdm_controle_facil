package com.example.projeto_final_pdm_controle_facil.model

import android.os.Parcel
import android.os.Parcelable

class Product (
    val id_produto     : Int? = null,
    var id_empresa     : Int?= null,
    var id_fornecedor  : Int?= null,
    var nome_produto   : String?= null,
    var preco          : Float?= null,
    var qtd_disponivel : Float?= null,
    var status         : Int?= null,
    var created_at     : String?= null,
    var updated_at     : String?= null,
) : Model(), Parcelable
{

    //==============================================================================================
    //                                      VALIDATION
    //==============================================================================================
    override fun validate() : Boolean{
        return  this.id_empresa.toString().isNotEmpty() &&
                (
                    this.id_fornecedor != -1
                ) &&
                (   this.nome_produto.toString().isNotEmpty() &&
                    this.nome_produto.toString().length <= 50
                ) &&
                (
                    this.preco.toString().isNotEmpty() &&
                    this.preco!! >= 0F
                ) &&
                (
                    this.qtd_disponivel.toString().isNotEmpty() &&
                    this.qtd_disponivel!! >= 0F
                ) &&
                this.status.toString().isNotEmpty()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    //to string ====================================================================================
    override fun toString(): String {
        return "Product(id_produto=$id_produto, id_empresa=$id_empresa, id_fornecedor=$id_fornecedor, nome_produto=$nome_produto, preco=$preco, qtd_disponivel=$qtd_disponivel, status=$status, created_at=$created_at, updated_at=$updated_at)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Product

        if (id_produto != other.id_produto) return false

        return true
    }

    override fun hashCode(): Int {
        return id_produto ?: 0
    }


    //==============================================================================================
    //                                       Parcelable
    //==============================================================================================

    constructor(parcel: Parcel) : this(
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readValue(Float::class.java.classLoader) as? Float,
        parcel.readValue(Float::class.java.classLoader) as? Float,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString()
    ) {
    }


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id_produto)
        parcel.writeValue(id_empresa)
        parcel.writeValue(id_fornecedor)
        parcel.writeString(nome_produto)
        parcel.writeValue(preco)
        parcel.writeValue(qtd_disponivel)
        parcel.writeValue(status)
        parcel.writeString(created_at)
        parcel.writeString(updated_at)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Product> {
        override fun createFromParcel(parcel: Parcel): Product {
            return Product(parcel)
        }

        override fun newArray(size: Int): Array<Product?> {
            return arrayOfNulls(size)
        }
    }


}