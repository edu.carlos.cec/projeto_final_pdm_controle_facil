package com.example.projeto_final_pdm_controle_facil.model

import java.time.LocalDateTime

class SellHistory (

    val id_venda    : Int?   = null,
    var id_produto  : Int?   = null,
    var qtd_venda   : Float? = null,
    var preco_total : Float? = null,
    var created_at  : String? = null,
    var updated_at  : String? = null,

) : Model(){
    override fun validate(): Boolean {
        TODO("Not yet implemented")
    }

    //==============================================================================================
    //==============================================================================================
    //==============================================================================================
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as SellHistory
        if (id_venda != other.id_venda) return false
        return true
    }

    override fun hashCode(): Int {
        return id_venda ?: 0
    }

    override fun toString(): String {
        return "HistoricoVenda(id_venda=$id_venda, id_produto=$id_produto, qtd_venda=$qtd_venda, preco_total=$preco_total, created_at=$created_at, updated_at=$updated_at)"
    }

    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


}