package com.example.projeto_final_pdm_controle_facil.model.data_classes

import java.lang.StringBuilder

data class ApiResponse(
    val mensagem : String,
)
