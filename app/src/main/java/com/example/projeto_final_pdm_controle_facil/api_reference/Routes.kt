package com.example.projeto_final_pdm_controle_facil.api_reference

enum class Routes(val path : String) {

    //==============================================================================================
    //============================== ROUTES FOR AUTHENTICATION ======================================
    //==============================================================================================
        //Routes for Users
        USER_AUTHENTICATION("auth/login"),

        //Routes for Users
        USER_LOGOUT("auth/logout"),

        //Route me
        USER_NAME("me"),

        USER_UPDATE("usuarios/usuario/"),
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    //Product Routes
    PRODUCTS_BY_COMPANY("produtos"),
    SPECIFIED_PRODUCT("produtos/"),
    ALL_PRODUCTS_BY_COMPANY("produtos/empresa/?/quantidade"),

    //Provider Routes
    PROVIDERS("fornecedores"),
    SPECIFIED_PROVIDER("fornecedores/"),


    //Company Routes
    USER_COMPANY("empresas/empresa/"),


    //History Routes
    HISTORY("historico-venda/"),
    HISTORY_AMOUNT_SEEL("historico-venda/total-amount-sell"),
    HISTORY_TOTAL_SEEL("historico-venda/total-sell"),
    HISTORY_TOTAL_PRODUCT_SEEL("historico-venda/total-product-sell")
}