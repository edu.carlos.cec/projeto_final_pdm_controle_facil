package com.example.projeto_final_pdm_controle_facil.ui.activities.provider

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.example.projeto_final_pdm_controle_facil.R
import com.example.projeto_final_pdm_controle_facil.api_reference.API
import com.example.projeto_final_pdm_controle_facil.api_reference.Routes
import com.example.projeto_final_pdm_controle_facil.global_data_control.ProductControl
import com.example.projeto_final_pdm_controle_facil.global_data_control.ProviderControl
import com.example.projeto_final_pdm_controle_facil.helper.GsonHelper
import com.example.projeto_final_pdm_controle_facil.http.JSONObjectRequestConcrete
import com.example.projeto_final_pdm_controle_facil.model.Product
import com.example.projeto_final_pdm_controle_facil.model.Provider
import com.example.projeto_final_pdm_controle_facil.ui.spinners.ProvidersSpinnerAdapter
import org.json.JSONObject
import kotlin.system.exitProcess

class InsertProviderActivity : AppCompatActivity() {

    //==============================================================================================
    //                                          VIEWS
    //==============================================================================================
    private lateinit var etxtProviderName    : TextView
    private lateinit var etxtProviderAddress : TextView
    private lateinit var progressBarInsertProvider : ProgressBar
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_insert_provider)


        //==========================================================================================
        this.etxtProviderName    = findViewById(R.id.etxtProviderName)
        this.etxtProviderAddress = findViewById(R.id.etxtProviderAddress)
        this.progressBarInsertProvider = findViewById(R.id.progressBarInsertProvider)
        //==========================================================================================
    }

    //==============================================================================================
    //                                      INSERT PROVIDER
    //==============================================================================================
    fun onClickInsertProvider(v : View){


        val provider = Provider(
            id_fornecedor = null,
            nome_fornecedor = this.etxtProviderName.text.toString().trim(),
            endereco_fornecedor = this.etxtProviderAddress.text.toString().trim(),
            created_at = null,
            updated_at = null
        )

        if( provider.validate() ){
            val json = GsonHelper().serialize(provider)
            val context = this
            JSONObjectRequestConcrete(
                this,
                Request.Method.POST,
                Routes.PROVIDERS.path,
                API.CURRENT.location,
                JSONObject(json),
                {
                    response ->
                        val provider = GsonHelper().deserialize<Provider>(response.toString()) as Provider
                        val pos : Int = ProviderControl.add(provider)
                        val intent = Intent().apply{
                            putExtra("POSITION", pos)
                        }
                        setResult(RESULT_OK, intent)
                        finish()
                },
                {
                    Toast.makeText(context, it.toString(), Toast.LENGTH_SHORT).show()
                }).performRequestWithAuthentication()
        } else {
            Toast.makeText(this, R.string.toast_insert_provider_error, Toast.LENGTH_SHORT).show()
        }

    }



    //==============================================================================================
    //                                          BACK BUTTON
    //==============================================================================================
    fun onClickBackButton(v : View){
        super.onBackPressed()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    //==============================================================================================
    //                                          LOGOUT BUTTON
    //==============================================================================================
    fun onLogoutButton(v : View){
        this.progressBarInsertProvider.visibility = View.VISIBLE
        JSONObjectRequestConcrete(
            this,
            Request.Method.POST,
            Routes.USER_LOGOUT.path,
            API.CURRENT.location,
            null,
            { _ ->
                this.progressBarInsertProvider.visibility = View.GONE
                moveTaskToBack(true);
                exitProcess(-1)
            },

            { _ ->
                this.progressBarInsertProvider.visibility = View.GONE
                Toast.makeText(this, R.string.toast_error_logout, Toast.LENGTH_SHORT).show()
            }).performRequestWithAuthentication()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================
}