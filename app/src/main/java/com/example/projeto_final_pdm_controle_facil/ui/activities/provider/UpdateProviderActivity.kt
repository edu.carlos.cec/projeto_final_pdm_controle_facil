package com.example.projeto_final_pdm_controle_facil.ui.activities.provider

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.example.projeto_final_pdm_controle_facil.R
import com.example.projeto_final_pdm_controle_facil.api_reference.API
import com.example.projeto_final_pdm_controle_facil.api_reference.Routes
import com.example.projeto_final_pdm_controle_facil.global_data_control.ProviderControl
import com.example.projeto_final_pdm_controle_facil.helper.GsonHelper
import com.example.projeto_final_pdm_controle_facil.http.JSONObjectRequestConcrete
import com.example.projeto_final_pdm_controle_facil.model.Provider
import org.json.JSONObject
import kotlin.system.exitProcess


class UpdateProviderActivity : AppCompatActivity() {

    //Views Activities =============================================================================
    private lateinit var etxtProviderName    : TextView
    private lateinit var etxtProviderAddress : TextView
    private lateinit var provider            : Provider
    private lateinit var progressBarUpdateProvider : ProgressBar
    //==============================================================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_provider)

        //==========================================================================================
        this.etxtProviderName    = findViewById(R.id.etxtProviderName)
        this.etxtProviderAddress = findViewById(R.id.etxtProviderAddress)
        this.provider            = intent.getParcelableExtra<Provider>("PROVIDER")!!
        this.progressBarUpdateProvider = findViewById(R.id.progressBarUpdateProvider)
        //==========================================================================================

        //==========================================================================================
        this.etxtProviderName.text    = this.provider.nome_fornecedor
        this.etxtProviderAddress.text = this.provider.endereco_fornecedor
        //==========================================================================================

    }


    fun onClickUpdateProvider(v : View){

        val provider = Provider(
            id_fornecedor = this.provider.id_fornecedor,
            nome_fornecedor = this.etxtProviderName.text.toString().trim(),
            endereco_fornecedor = this.etxtProviderAddress.text.toString().trim(),
            created_at = null,
            updated_at = null
        )


        if( provider.validate() ) {

            val providerJson : String = GsonHelper().serialize(provider)
            JSONObjectRequestConcrete(
                this,
                Request.Method.PUT,
                Routes.SPECIFIED_PROVIDER.path + this.provider.id_fornecedor,
                API.CURRENT.location,
                JSONObject(providerJson),
                { _ ->
                    Toast.makeText(baseContext, R.string.toast_update_success, Toast.LENGTH_SHORT).show()
                    val response = Intent().apply{
                        putExtra("FORNECEDOR_RESPOSTA", provider)
                    }
                    setResult(RESULT_OK, response)
                    finish()
                },
                {
                    Toast.makeText(baseContext, it.toString(), Toast.LENGTH_SHORT).show()
                }).performRequestWithAuthentication()
            //==========================================================================================
        } else {
            Toast.makeText(this, R.string.toast_update_error, Toast.LENGTH_SHORT).show()
        }

    }


    //==============================================================================================
    //                                          BACK BUTTON
    //==============================================================================================
    fun onClickBackButton(v : View){
        super.onBackPressed()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    //==============================================================================================
    //                                          LOGOU BUTTON
    //==============================================================================================
    fun onLogoutButton(v : View){
        this.progressBarUpdateProvider.visibility = View.VISIBLE
        JSONObjectRequestConcrete(
            this,
            Request.Method.POST,
            Routes.USER_LOGOUT.path,
            API.CURRENT.location,
            null,
            { _ ->
                this.progressBarUpdateProvider.visibility = View.GONE
                moveTaskToBack(true);
                exitProcess(-1)
            },

            { _ ->
                this.progressBarUpdateProvider.visibility = View.GONE
                Toast.makeText(this, R.string.toast_error_logout, Toast.LENGTH_SHORT).show()
            }).performRequestWithAuthentication()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================
}