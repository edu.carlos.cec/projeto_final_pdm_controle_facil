package com.example.projeto_final_pdm_controle_facil.ui.activities.financies

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.example.projeto_final_pdm_controle_facil.R
import com.example.projeto_final_pdm_controle_facil.api_reference.API
import com.example.projeto_final_pdm_controle_facil.api_reference.Routes
import com.example.projeto_final_pdm_controle_facil.global_data_control.CompanyControl
import com.example.projeto_final_pdm_controle_facil.global_data_control.FinanceControl
import com.example.projeto_final_pdm_controle_facil.global_data_control.ProductControl
import com.example.projeto_final_pdm_controle_facil.global_data_control.UserControl
import com.example.projeto_final_pdm_controle_facil.helper.GsonHelper
import com.example.projeto_final_pdm_controle_facil.http.JSONArrayRequestConcrete
import com.example.projeto_final_pdm_controle_facil.http.JSONObjectRequestConcrete
import com.example.projeto_final_pdm_controle_facil.model.Product
import com.example.projeto_final_pdm_controle_facil.model.SellHistory
import com.example.projeto_final_pdm_controle_facil.ui.activities.products.UpdateProductActivity
import com.example.projeto_final_pdm_controle_facil.ui.activities.reports.MainReportsActivity
import com.example.projeto_final_pdm_controle_facil.ui.alertBuilder.DeleteDialog
import com.example.projeto_final_pdm_controle_facil.ui.alertBuilder.EditQuantityDialog
import com.example.projeto_final_pdm_controle_facil.ui.lists.finance.FinanceListAdapter
import com.example.projeto_final_pdm_controle_facil.ui.lists.product.ProductListAdapter
import org.json.JSONObject
import kotlin.system.exitProcess

class MainFinanciesActivity : AppCompatActivity() {

    data class TotalSell(
        val total: Int
    )

    //Atributos da Activity=========================================================================
    private lateinit var userName            : TextView
    private lateinit var companyName         : TextView
    private lateinit var total_amount_sell   : TextView
    private lateinit var total_sell          : TextView
    private lateinit var total_sell_products : TextView
    private lateinit var rvSell              : RecyclerView
    private lateinit var progressBarFinances : ProgressBar
    //==============================================================================================


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_financies)


        //Iniciando os valores =====================================================================
        this.total_amount_sell    = findViewById(R.id.total_amount_sell)
        this.total_sell           = findViewById(R.id.total_sell)
        this.total_sell_products  = findViewById(R.id.total_sell_products)
        this.rvSell               = findViewById(R.id.rvSell)
        this.userName             = findViewById(R.id.userName)
        this.companyName          = findViewById(R.id.companyName)
        this.progressBarFinances  = findViewById(R.id.progressBarFinances)
        //==========================================================================================


        //==========================================================================================
        this.userName.text    = UserControl.getUserAuth().name
        this.companyName.text = CompanyControl.getUserCompany().nome_empresa
        //==========================================================================================

        this.progressBarFinances.visibility = View.VISIBLE

        //==========================================================================================
        ////=============================== TOTAL VENDIDO ==========================================
        //==========================================================================================
        JSONObjectRequestConcrete(this,
            Request.Method.GET,
            Routes.HISTORY_AMOUNT_SEEL.path,
            API.CURRENT.location,
            null,
            { response ->
                val obj =
                    GsonHelper().deserialize<TotalSell>(response.toString()) as TotalSell
                this.total_amount_sell.text = obj.total.toString()
            },
            {
                Toast.makeText(baseContext, it.toString(), Toast.LENGTH_LONG).show()
            }).performRequestWithAuthentication()
        //==========================================================================================
        //==========================================================================================
        //==========================================================================================


        //==========================================================================================
        //=============================== QUANTIDADE DE VENDAS =====================================
        //==========================================================================================
        JSONObjectRequestConcrete(this,
            Request.Method.GET,
            Routes.HISTORY_TOTAL_SEEL.path,
            API.CURRENT.location,
            null,
            { response ->
                val obj =
                    GsonHelper().deserialize<TotalSell>(response.toString()) as TotalSell
                this.total_sell.text = obj.total.toString()
            },
            {
                Toast.makeText(baseContext, it.toString(), Toast.LENGTH_LONG).show()
            }).performRequestWithAuthentication()
        //==========================================================================================
        //==========================================================================================
        //==========================================================================================


        //==========================================================================================
        //=============================== PRODUTOS DE VENDAS =======================================
        //==========================================================================================
        JSONObjectRequestConcrete(this,
            Request.Method.GET,
            Routes.HISTORY_TOTAL_PRODUCT_SEEL.path,
            API.CURRENT.location,
            null,
            { response ->
                val obj =
                    GsonHelper().deserialize<TotalSell>(response.toString()) as TotalSell
                this.total_sell_products.text = obj.total.toString()
            },
            {
                Toast.makeText(baseContext, it.toString(), Toast.LENGTH_LONG).show()
            }).performRequestWithAuthentication()
        //==========================================================================================
        //==========================================================================================
        //==========================================================================================


        //==========================================================================================
        //==========================================================================================
        //==========================================================================================
        JSONArrayRequestConcrete(this,
            Request.Method.GET,
            Routes.HISTORY.path,
            API.CURRENT.location,
            null,
            { response ->
                val finances  = GsonHelper().deserializeArray<SellHistory>(response.toString()) as ArrayList<SellHistory>
                FinanceControl.addAllSells(finances)
                rvSell.layoutManager = LinearLayoutManager(this)
                rvSell.setHasFixedSize(true)
                rvSell.adapter       = FinanceListAdapter(FinanceControl.getAllSells())

                this.progressBarFinances.visibility = View.GONE
            },
            {
                Toast.makeText(baseContext, it.toString(), Toast.LENGTH_SHORT).show()
            }).performRequestWithAuthentication()
        //==========================================================================================
        //==========================================================================================
        //==========================================================================================


    }


    //==============================================================================================
    //==============================================================================================
    //==============================================================================================
    fun onClickBackButton(v : View){
        super.onBackPressed()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    fun onLogoutButton(v : View){

        this.progressBarFinances.visibility = View.VISIBLE

        JSONObjectRequestConcrete(
            this,
            Request.Method.POST,
            Routes.USER_LOGOUT.path,
            API.CURRENT.location,
            null,
            { _ ->
                this.progressBarFinances.visibility = View.GONE
                moveTaskToBack(true);
                exitProcess(-1)
            },

            { _ ->
                this.progressBarFinances.visibility = View.GONE
                Toast.makeText(this, R.string.toast_error_logout, Toast.LENGTH_SHORT).show()
            }).performRequestWithAuthentication()


    }
}