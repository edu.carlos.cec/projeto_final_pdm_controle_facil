package com.example.projeto_final_pdm_controle_facil.ui.spinners

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.projeto_final_pdm_controle_facil.R
import com.example.projeto_final_pdm_controle_facil.model.Provider
import org.w3c.dom.Text

class ProvidersSpinnerAdapter(

    context: Context,
    private var textViewResourceID : Int,
    private var providers : ArrayList<Provider>

) : ArrayAdapter<Provider>(context, textViewResourceID, providers) {

    override fun getCount(): Int {
        return this.providers.size
    }

    override fun getItem(position: Int): Provider? {
        return providers.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val cview = if(convertView == null){

            //Instaciar o layout
            val inflater = LayoutInflater.from(parent!!.context)
            inflater.inflate(textViewResourceID, parent, false)

        } else {
            convertView
        }

        val txtIdProvider : TextView = cview.findViewById(R.id.idProvider)
        val txtNameProvider : TextView = cview.findViewById(R.id.nameProvider)
        txtIdProvider.text = this.providers.get(position).id_fornecedor.toString()
        txtNameProvider.text = this.providers.get(position).nome_fornecedor
        return cview
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val cview = if(convertView == null){

            //Instaciar o layout
            val inflater = LayoutInflater.from(parent!!.context)
            inflater.inflate(textViewResourceID, parent, false)

        } else {
            convertView
        }

        val txtIdProvider : TextView = cview.findViewById(R.id.idProvider)
        val txtNameProvider : TextView = cview.findViewById(R.id.nameProvider)
        txtIdProvider.text = this.providers.get(position).id_fornecedor.toString()
        txtNameProvider.text = this.providers.get(position).nome_fornecedor
        return cview
    }
}