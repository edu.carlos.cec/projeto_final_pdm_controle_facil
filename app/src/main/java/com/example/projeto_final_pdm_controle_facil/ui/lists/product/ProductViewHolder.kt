package com.example.projeto_final_pdm_controle_facil.ui.lists.product

import android.view.View
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.projeto_final_pdm_controle_facil.R
import com.example.projeto_final_pdm_controle_facil.model.Product


//O itemView é como toda a View
//O construtor da ViewHolder deve sempre definir seu itemView para a superclasse
//Recebemos o adapter para ter acesso ao listener
class ProductViewHolder(

    itemView : View,
    private val adapter: ProductListAdapter

) : RecyclerView.ViewHolder(itemView){

    //Aqui temos todas as views que precisa ser guardadas
    private val idProduct           : TextView  = itemView.findViewById(R.id.id_sell)
    private val nameProduct         : TextView  = itemView.findViewById(R.id.name_product)
    private val priceProduct        : TextView  = itemView.findViewById(R.id.price_product)
    private val quantityProduct     : TextView  = itemView.findViewById(R.id.quantity_product)
    private val status              : TextView  = itemView.findViewById(R.id.status_product)
    private val dataCreationProduct : TextView  = itemView.findViewById(R.id.date_creation)
    private val dataUpdateProduct   : TextView  = itemView.findViewById(R.id.date_update)
    private val addToHistory        : ImageView = itemView.findViewById(R.id.addToHistory)
    private val edit_product        : ImageView = itemView.findViewById(R.id.edit_product)
    private val delete_product      : ImageView = itemView.findViewById(R.id.delete_product)

    private lateinit var product : Product //Mantem a informacao da Task


    //Vincula a task com as Views
    //Aqui acontece a reciclagem da ViewHolder
    //Será chamado toda vez que houver necessidade de reciclar os dados da itemView
    fun bind(product: Product, position : Int) {
        this.product = product
        this.idProduct.text = product.id_produto.toString()
        this.nameProduct.text = product.nome_produto
        this.priceProduct.text = product.preco.toString()
        this.quantityProduct.text = product.qtd_disponivel.toString()
        this.status.text = if (product.status == 1) {
            "Disponível"
        } else {
            "Não Disponível"
        }
        this.dataCreationProduct.text = product.created_at
        this.dataUpdateProduct.text = product.updated_at

        this.edit_product.setOnClickListener{this.onClickUpdate()} //Disparando o evento de update
        this.delete_product.setOnClickListener{this.onClickDelete()} //Disparando o evento de delete
        this.addToHistory.setOnClickListener{this.onClickAddHIstory()}
    }

    //O onClick executa onClick nosso
    private fun onClickDelete() {
        this.adapter.getOnClickDeleteListener()?.onClick(this.product) //Se o ONCLICK for diferente de null
    }

    private fun onClickUpdate() {
        this.adapter.getOnClickUpdateListener()?.onClick(this.product) //se o ONCLICK for diferente de null
    }

    private fun onClickAddHIstory() {
        this.adapter.getOnClickAddToHistory()?.onClick(this.product) //se o ONCLICK for diferente de null
    }




    //==============================================================================================
    //                           EVENTOS DENTRO DO EDITAR PRODUTO
    //==============================================================================================

    fun onClickPlus(v : View?, quantity : TextView){
        quantity.text = (quantity.text.toString().toInt() + 1).toString()
    }

    fun onClickMinus(v : View?, quantity : TextView){
        if(quantity.text.toString().toInt() == 0){
            Toast.makeText(v?.context, "Não há como tirar mais!", Toast.LENGTH_SHORT).show()
        }else {
            quantity.text = (quantity.text.toString().toInt() - 1).toString()
        }
    }
    //==============================================================================================

}