package com.example.projeto_final_pdm_controle_facil.ui.activities.user

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.example.projeto_final_pdm_controle_facil.R
import com.example.projeto_final_pdm_controle_facil.api_reference.API
import com.example.projeto_final_pdm_controle_facil.api_reference.Routes
import com.example.projeto_final_pdm_controle_facil.global_data_control.UserControl
import com.example.projeto_final_pdm_controle_facil.helper.GsonHelper
import com.example.projeto_final_pdm_controle_facil.http.JSONObjectRequestConcrete
import com.example.projeto_final_pdm_controle_facil.model.User
import org.json.JSONObject
import kotlin.system.exitProcess

class UpdateUserActivity : AppCompatActivity() {

    //==============================================================================================
    //==============================================================================================
    //==============================================================================================
    data class UserUpdate(
        val name              : String? = null,
        val email             : String? = null,
        val password          : String? = null,
        val updated_at        : String? = null
    )
    //==============================================================================================


    //==============================================================================================
    //                                          VIEWS
    //==============================================================================================
    private lateinit var etxtNameUser           : TextView
    private lateinit var etxtEmail              : TextView
    private lateinit var etxtPassword           : TextView
    private lateinit var backButton             : ImageView
    private lateinit var progressBarUpdateUser  : ProgressBar
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_user)

        //Pegar os valores das views do usuário ====================================================
        this.etxtNameUser          = findViewById(R.id.etxtNameUser)
        this.etxtEmail             = findViewById(R.id.etxtEmail)
        this.etxtPassword          = findViewById(R.id.etxtPassword)
        this.backButton            = findViewById(R.id.backButton)
        this.progressBarUpdateUser = findViewById(R.id.progressBarUpdateUser)
        //==========================================================================================

        this.etxtNameUser.text = UserControl.getUserAuth().name.toString()
        this.etxtEmail.text    = UserControl.getUserAuth().email.toString()
    }



    //==============================================================================================
    //                                          ON UPDATE USER
    //==============================================================================================
    fun onUpdateButton(v : View){

        var user = User(
            name = this.etxtNameUser.text.toString(),
            email = this.etxtEmail.text.toString(),
            password =  if( this.etxtPassword.text.toString().isEmpty() ) { null }
                        else { this.etxtPassword.text.toString() },
            updated_at = null
        )

        if( user.validate() ) {

            val json = GsonHelper().serialize(user)
            JSONObjectRequestConcrete(
                this,
                Request.Method.PUT,
                Routes.USER_UPDATE.path + UserControl.getUserAuth().id_usuario,
                API.CURRENT.location,
                JSONObject(json),
                { _ ->
                    UserControl.getUserAuth().name      = user.name
                    UserControl.getUserAuth().email     = user.email
                    UserControl.getUserAuth().password  = user.name
                    val response = Intent().apply {
                        putExtra("USUARIO_RESPOSTA", 1)
                    }
                    setResult(RESULT_OK, response)
                    finish()
                },
                {
                    Toast.makeText(baseContext, it.toString(), Toast.LENGTH_SHORT).show()
                }).performRequestWithAuthentication()

        }

    }

    //==============================================================================================
    //                                      BACK BUTTON
    //==============================================================================================
    fun onClickBackButton(v : View){
        super.onBackPressed()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================



    //==============================================================================================
    //                                      LOGOUT BUTTON
    //==============================================================================================
    fun onLogoutButton(v : View){
        this.progressBarUpdateUser.visibility = View.VISIBLE
        JSONObjectRequestConcrete(
            this,
            Request.Method.POST,
            Routes.USER_LOGOUT.path,
            API.CURRENT.location,
            null,
            { _ ->
                this.progressBarUpdateUser.visibility = View.GONE
                moveTaskToBack(true);
                exitProcess(-1)
            },

            { _ ->
                this.progressBarUpdateUser.visibility = View.GONE
                Toast.makeText(this, R.string.toast_error_logout, Toast.LENGTH_SHORT).show()
            }).performRequestWithAuthentication()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================
}