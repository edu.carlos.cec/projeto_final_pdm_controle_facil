package com.example.projeto_final_pdm_controle_facil.ui.lists.finance

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.projeto_final_pdm_controle_facil.R
import com.example.projeto_final_pdm_controle_facil.model.SellHistory

//Criação do RecyclerView de produto
class FinanceListAdapter(

    private val financeList : ArrayList<SellHistory>

) : RecyclerView.Adapter<FinanceViewHolder>(){


    //==============================================================================================
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FinanceViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.default_finance_layout, parent, false)
        return FinanceViewHolder(itemView, this)
    }

    //Recicla os valores que estao na itemView, usando o ViewHolder
    override fun onBindViewHolder(holder: FinanceViewHolder, position: Int) {
        holder.bind(this.financeList[position], position)
    }

    override fun getItemCount(): Int {
        return this.financeList.size
    }
    //==============================================================================================


}
