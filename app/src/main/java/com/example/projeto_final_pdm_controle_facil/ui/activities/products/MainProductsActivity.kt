package com.example.projeto_final_pdm_controle_facil.ui.activities.products

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.example.projeto_final_pdm_controle_facil.R
import com.example.projeto_final_pdm_controle_facil.api_reference.API
import com.example.projeto_final_pdm_controle_facil.api_reference.Routes
import com.example.projeto_final_pdm_controle_facil.global_data_control.CompanyControl
import com.example.projeto_final_pdm_controle_facil.global_data_control.ProductControl
import com.example.projeto_final_pdm_controle_facil.global_data_control.UserControl
import com.example.projeto_final_pdm_controle_facil.helper.GsonHelper
import com.example.projeto_final_pdm_controle_facil.http.JSONArrayRequestConcrete
import com.example.projeto_final_pdm_controle_facil.http.JSONObjectRequestConcrete
import com.example.projeto_final_pdm_controle_facil.model.SellHistory
import com.example.projeto_final_pdm_controle_facil.model.Product
import com.example.projeto_final_pdm_controle_facil.ui.alertBuilder.DeleteDialog
import com.example.projeto_final_pdm_controle_facil.ui.alertBuilder.EditQuantityDialog
import com.example.projeto_final_pdm_controle_facil.ui.lists.product.ProductListAdapter
import org.json.JSONObject
import kotlin.system.exitProcess

class MainProductsActivity : AppCompatActivity() {

    //Values of Activities =========================================================================
    private lateinit var userName                  : TextView
    private lateinit var companyName               : TextView
    private lateinit var rvProductList             : RecyclerView
    private lateinit var search_product            : EditText
    private lateinit var resultLauncher            : ActivityResultLauncher<Intent>
    private lateinit var resultLauncherUpdate      : ActivityResultLauncher<Intent>
    private lateinit var progressBarMainProduct    : ProgressBar
    //==============================================================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_products)

        //Initializating the values ================================================================
        this.userName       = findViewById(R.id.userName)
        this.companyName    = findViewById(R.id.companyName)
        this.rvProductList  = findViewById(R.id.rvProviderList)
        this.search_product = findViewById(R.id.search_provider)
        this.progressBarMainProduct = findViewById(R.id.progressBarMainProduct)
        //==========================================================================================


        //==========================================================================================
        this.userName.text    = UserControl.getUserAuth().name
        this.companyName.text = CompanyControl.getUserCompany().nome_empresa
        //==========================================================================================


        //==========================================================================================
        //                                  CHANGE OF EDIT TEXT
        //==========================================================================================
        val context = this
        val rvListAdapter = this.rvProductList


        this.search_product.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s!!.isNotEmpty()){
                    (rvListAdapter.adapter as ProductListAdapter).filterProduct(true, s.toString())
                } else {
                    (rvListAdapter.adapter as ProductListAdapter).filterProduct(false, s.toString())
                }

            }
        })
        //==========================================================================================
        //==========================================================================================
        //==========================================================================================




        JSONArrayRequestConcrete(this,
            Request.Method.GET,
            Routes.PRODUCTS_BY_COMPANY.path,
            API.CURRENT.location,
            null,
        {
            response ->
                val products = GsonHelper().deserializeArray<Product>(response.toString())
                ProductControl.addAllProducts(products)

                rvListAdapter.layoutManager = LinearLayoutManager(context)
                rvListAdapter.setHasFixedSize(true)
                rvListAdapter.adapter       = ProductListAdapter(ProductControl.getAllProducts())

                    .setOnClickDeleteProductListener{

                        object : DeleteDialog(this){

                            override fun delete() {
                                JSONObjectRequestConcrete(
                                    baseContext,
                                    Request.Method.DELETE,
                                    Routes.SPECIFIED_PRODUCT.path + it?.id_produto,
                                    API.CURRENT.location,
                                    null,
                                    {
                                        _ ->
                                            Toast.makeText(context, R.string.toast_delete_success, Toast.LENGTH_SHORT).show()
                                            //Atualizar o Singleton
                                            val pos = ProductControl.delete(it!!)
                                            rvListAdapter.adapter?.notifyItemRemoved(pos)
                                    },
                                    {
                                        Toast.makeText(context,  R.string.toast_delete_error, Toast.LENGTH_SHORT).show()
                                    }
                                ).performRequestWithAuthentication()
                            }
                        }
                }
                    .setOnClickUpdateProductListener{
                        val requestProductUpdate = Intent(this, UpdateProductActivity::class.java).apply {
                            putExtra("PRODUTO", it)
                        }
                        this.resultLauncherUpdate.launch(requestProductUpdate)
                }
                    .setOnClickAddToHistory {

                        object : EditQuantityDialog(this){
                            override fun onUpdate(quantity: Int) {
                                if (quantity == 0 || quantity < 0) {

                                    Toast.makeText(
                                        baseContext,
                                        R.string.toast_update_quantity_error,
                                        Toast.LENGTH_SHORT
                                    ).show()

                                } else {
                                    it?.qtd_disponivel = it?.qtd_disponivel?.minus(quantity.toFloat())
                                    val pos = ProductControl.update(it!!)
                                    rvListAdapter.adapter?.notifyItemChanged(pos)

                                    val quantityUpdateJson: JSONObject? = JSONObject()
                                    quantityUpdateJson?.put("qtd_disponivel", it.qtd_disponivel)

                                    JSONObjectRequestConcrete(
                                        baseContext,
                                        Request.Method.PUT,
                                        Routes.SPECIFIED_PRODUCT.path + it.id_produto,
                                        API.CURRENT.location,
                                        quantityUpdateJson,
                                        { _ ->
                                            Toast.makeText(
                                                baseContext,
                                                R.string.toast_update_success,
                                                Toast.LENGTH_SHORT
                                            ).show()

                                            //Montar o objeto de Histórico =============================
                                            val venda = SellHistory(
                                                id_venda = null,
                                                id_produto = it.id_produto,
                                                qtd_venda = quantity.toFloat(),
                                                preco_total = quantity.toFloat() * it.preco!!.toFloat(),
                                                created_at = null,
                                                updated_at = null
                                            )
                                            val json = GsonHelper().serialize(venda)
                                            JSONObjectRequestConcrete(
                                                baseContext,
                                                Request.Method.POST,
                                                Routes.HISTORY.path,
                                                API.CURRENT.location,
                                                JSONObject(json),
                                                { _ ->
                                                    Toast.makeText(
                                                        baseContext,
                                                        R.string.toast_insert_history_success,
                                                        Toast.LENGTH_SHORT
                                                    ).show()
                                                },
                                                {
                                                    Toast.makeText(
                                                        baseContext,
                                                        it.toString(),
                                                        Toast.LENGTH_SHORT
                                                    ).show()
                                                }).performRequestWithAuthentication()
                                            //==========================================================

                                        },
                                        {
                                            Toast.makeText(
                                                baseContext,
                                                it.toString(),
                                                Toast.LENGTH_SHORT
                                            ).show()
                                        }).performRequestWithAuthentication()
                                }
                            }
                        }
                }
        },
        {
            Toast.makeText(context, it.toString(), Toast.LENGTH_SHORT).show()
        }).performRequestWithAuthentication()

        //==========================================================================================


        buildResultLauncher() //Criar o resultLaucher do insert
        buildResultLauncherUpdate() //Criar o resultLaucher do update

    }


    //==============================================================================================
    //                                      INSERT PRODUCT
    //==============================================================================================
    fun onClickInsertProduct(v : View){
        val requestProduct = Intent(this, InsertProductActivity::class.java)
        this.resultLauncher.launch(requestProduct)
    }

    private fun buildResultLauncher(){
        val contract = ActivityResultContracts.StartActivityForResult()
        this.resultLauncher = registerForActivityResult(contract){
            result ->
                if(result.resultCode == RESULT_OK && result.data != null){
                    val pos = result.data!!.getIntExtra("POSITION", -1)
                    rvProductList.adapter?.notifyItemInserted(pos)
                    Toast.makeText(
                        baseContext,
                        R.string.toast_insert_product_success,
                        Toast.LENGTH_SHORT
                    ).show()
                }
        }
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================



    //==============================================================================================
    //                                  UPDATE PRODUCT
    //==============================================================================================
    private fun buildResultLauncherUpdate(){
        val contract = ActivityResultContracts.StartActivityForResult()
        this.resultLauncherUpdate = registerForActivityResult(contract){
                result ->
            if(result.resultCode == RESULT_OK && result.data != null){
                val prod = result.data!!.getParcelableExtra<Product>("PRODUTO_RESPOSTA")
                val pos = ProductControl.update(prod!!)
                rvProductList.adapter?.notifyItemChanged(pos)
            }
        }
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    //==============================================================================================
    //                                      BACK BUTTON
    //==============================================================================================
    fun onClickBackButton(v : View){
        super.onBackPressed()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================



    //==============================================================================================
    //                                      LOGOUT BUTTON
    //==============================================================================================
    fun onLogoutButton(v : View){
        this.progressBarMainProduct.visibility = View.VISIBLE
        JSONObjectRequestConcrete(
            this,
            Request.Method.POST,
            Routes.USER_LOGOUT.path,
            API.CURRENT.location,
            null,
            { _ ->
                this.progressBarMainProduct.visibility = View.GONE
                moveTaskToBack(true);
                exitProcess(-1)
            },

            { _ ->
                this.progressBarMainProduct.visibility = View.GONE
                Toast.makeText(this, R.string.toast_error_logout, Toast.LENGTH_SHORT).show()
            }).performRequestWithAuthentication()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================

}

