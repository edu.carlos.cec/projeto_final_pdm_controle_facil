package com.example.projeto_final_pdm_controle_facil.ui.activities.reports

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.example.projeto_final_pdm_controle_facil.R
import com.example.projeto_final_pdm_controle_facil.api_reference.API
import com.example.projeto_final_pdm_controle_facil.api_reference.Routes
import com.example.projeto_final_pdm_controle_facil.global_data_control.CompanyControl
import com.example.projeto_final_pdm_controle_facil.global_data_control.ProductControl
import com.example.projeto_final_pdm_controle_facil.global_data_control.ProviderControl
import com.example.projeto_final_pdm_controle_facil.global_data_control.UserControl
import com.example.projeto_final_pdm_controle_facil.helper.GsonHelper
import com.example.projeto_final_pdm_controle_facil.http.JSONArrayRequestConcrete
import com.example.projeto_final_pdm_controle_facil.http.JSONObjectRequestConcrete
import com.example.projeto_final_pdm_controle_facil.model.Product
import com.example.projeto_final_pdm_controle_facil.model.Provider
import com.example.projeto_final_pdm_controle_facil.pdf.PDFDirectory
import com.example.projeto_final_pdm_controle_facil.ui.activities.products.UpdateProductActivity
import com.example.projeto_final_pdm_controle_facil.ui.activities.provider.UpdateProviderActivity
import com.example.projeto_final_pdm_controle_facil.ui.alertBuilder.DeleteDialog
import com.example.projeto_final_pdm_controle_facil.ui.alertBuilder.EditQuantityDialog
import com.example.projeto_final_pdm_controle_facil.ui.lists.product.ProductListAdapter
import com.example.projeto_final_pdm_controle_facil.ui.lists.provider.ProviderListAdapter
import com.itextpdf.text.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import com.itextpdf.text.pdf.PdfWriter
import com.itextpdf.text.pdf.BaseFont

import com.itextpdf.text.pdf.draw.LineSeparator
import com.itextpdf.text.Paragraph

import com.itextpdf.text.Chunk
import org.json.JSONObject
import com.itextpdf.text.BaseColor
import com.itextpdf.text.pdf.PdfPCell
import com.itextpdf.text.pdf.PdfPTable
import org.w3c.dom.Text
import java.io.OutputStream
import java.net.URL
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*
import kotlin.system.exitProcess


class MainReportsActivity : AppCompatActivity() {

    data class TotalProducts(
        val total: Int
    )

    //==============================================================================================
    //                                          VIEWS
    //==============================================================================================
    private lateinit var backButton    : ImageView
    private lateinit var userName      : TextView
    private lateinit var companyName   : TextView
    private lateinit var totalProducts : TextView
    private lateinit var totalProvider : TextView
    private lateinit var progressBarReport : ProgressBar
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_reports)

        //==========================================================================================
        //                                  VIEWS
        //==========================================================================================
        this.backButton    = findViewById(R.id.backButton)
        this.totalProducts = findViewById(R.id.total_products)
        this.totalProvider = findViewById(R.id.total_provider)
        this.userName      = findViewById(R.id.userName)
        this.companyName   = findViewById(R.id.companyName)
        this.progressBarReport   = findViewById(R.id.progressBarReport)
        //==========================================================================================
        //==========================================================================================
        //==========================================================================================


        //==========================================================================================
        this.userName.text    = UserControl.getUserAuth().name
        this.companyName.text = CompanyControl.getUserCompany().nome_empresa
        //==========================================================================================

        this.progressBarReport.visibility = View.VISIBLE
        JSONObjectRequestConcrete(
            this,
            Request.Method.GET,
            "produtos/empresa/${CompanyControl.company.id_empresa}/quantidade",
            API.CURRENT.location,
            null,
            { response ->
                val obj =
                    GsonHelper().deserialize<TotalProducts>(response.toString()) as TotalProducts
                    this.totalProducts.text = obj.total.toString()
            },
            {
                Toast.makeText(baseContext, it.toString(), Toast.LENGTH_SHORT).show()
            }).performRequestWithAuthentication()



        JSONObjectRequestConcrete(
            this,
            Request.Method.GET,
            "fornecedores/quantidade/fornecedor",
            API.CURRENT.location,
            null,
            { response ->
                val obj =
                    GsonHelper().deserialize<TotalProducts>(response.toString()) as TotalProducts
                this.totalProvider.text = obj.total.toString()
            },
            {
                Toast.makeText(baseContext, it.toString(), Toast.LENGTH_SHORT).show()
            }).performRequestWithAuthentication()


        JSONArrayRequestConcrete(this,
            Request.Method.GET,
            Routes.PRODUCTS_BY_COMPANY.path,
            API.CURRENT.location,
            null,
            { response ->
                    val products = GsonHelper().deserializeArray<Product>(response.toString())
                    ProductControl.addAllProducts(products)
            },
            {
                Toast.makeText(baseContext, it.toString(), Toast.LENGTH_SHORT).show()
            }).performRequestWithAuthentication()


        JSONArrayRequestConcrete(
            this,
            Request.Method.GET,
            Routes.PROVIDERS.path,
            API.CURRENT.location,
            null,
            { response ->
                val providers = GsonHelper().deserializeArray<Provider>(response.toString())
                ProviderControl.addAllProviders(providers)
                this.progressBarReport.visibility = View.GONE
            },
            {
                Toast.makeText(baseContext, it.toString(), Toast.LENGTH_SHORT).show()
            }).performRequestWithAuthentication()
    }






    @RequiresApi(Build.VERSION_CODES.Q)
    fun PDFAllProviders(v: View) {


        this.progressBarReport.visibility = View.VISIBLE


        val document = Document()
        //Criando um formatador para a data
        val s = SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
        //Diretorio de documentos do app
        val file = File(this.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), "AllProviders-${s.format(Date())}.pdf")
        PdfWriter.getInstance(document, FileOutputStream(file))
        PdfWriter.getInstance(document, FileOutputStream(file))
        document.open()

        //==========================================================================================
        //                                  Document Settings
        //==========================================================================================
        document.pageSize = PageSize.A4
        document.addCreationDate()
        document.addCreator("Controle Fácil")
        //==========================================================================================

        val mColorAccent = BaseColor(0, 153, 204, 255)


        //==========================================================================================
        //Fazendo o cabeçalho do formulário ========================================================
        //==========================================================================================

        val title = Paragraph(5F, "Relatório de todos os Fornecedores", Font(Font.FontFamily.HELVETICA, 24.0f, Font.BOLD, BaseColor.BLACK))
        title.alignment = Element.ALIGN_CENTER
        title.spacingBefore = 50F
        document.add(title)


        val title2 = Paragraph(10F, "Todos os fornecedores disponíveis em sua empresa", Font(Font.FontFamily.HELVETICA, 12.0f, Font.NORMAL, BaseColor.BLACK))
        title2.alignment     = Element.ALIGN_CENTER
        title2.spacingAfter  = 50F
        title2.spacingBefore = 50F
        document.add(title2)

        //==========================================================================================
        //==========================================================================================
        //==========================================================================================

        //==========================================================================================
        //Fazendo o cabeçalho do formulário ========================================================
        //==========================================================================================
        val cellName    = PdfPCell(Phrase("Nome Usuário:"))
        val cellEmpresa = PdfPCell(Phrase("Empresa"))
        val cellEmail   = PdfPCell(Phrase("Email"))
        val cellData    = PdfPCell(Phrase("Data"))
        val table = PdfPTable(4)
        table.paddingTop = 20F
        table.addCell(cellName)
        table.addCell(cellEmpresa)
        table.addCell(cellEmail)
        table.addCell(cellData)

        table.addCell(UserControl.getUserAuth().name)
        table.addCell(CompanyControl.getUserCompany().nome_empresa)
        table.addCell(UserControl.getUserAuth().email)
        table.addCell(LocalDate.now().toString())

        document.add(table)
        //==========================================================================================
        //==========================================================================================
        //==========================================================================================


        //==========================================================================================
        //Fazendo o cabeçalho do formulário ========================================================
        //==========================================================================================
        val subTitle = Paragraph("FORNECEDORES")
        subTitle.alignment = Element.ALIGN_CENTER
        subTitle.font = Font(Font.FontFamily.COURIER)
        subTitle.spacingAfter  = 50F
        subTitle.spacingBefore = 50F
        document.add(subTitle)
        //==========================================================================================
        //==========================================================================================
        //==========================================================================================

        // LINE SEPARATOR
        val lineSeparator = LineSeparator()
        lineSeparator.lineColor = BaseColor.BLACK

        ProviderControl.getAllProviders().forEach {

            document.add(lineSeparator)

            var attr  = PdfPCell()
            attr.border = Rectangle.NO_BORDER
            var value = PdfPCell()
            value.border = Rectangle.NO_BORDER
            var tableProvider = PdfPTable(2)
            tableProvider.addCell(attr)
            tableProvider.addCell(value)

            var cell1 = PdfPCell()
            var cell2 = PdfPCell()

            tableProvider.spacingBefore = 20F

            cell1.phrase = Phrase("ID")
            cell1.border = Rectangle.NO_BORDER
            tableProvider.addCell(cell1)
            cell2.phrase = Phrase(it.id_fornecedor.toString())
            cell2.border = Rectangle.NO_BORDER
            tableProvider.addCell(cell2)

            cell1.phrase = Phrase("NOME")
            cell1.border = Rectangle.NO_BORDER
            tableProvider.addCell(cell1)
            cell2.phrase = Phrase(it.nome_fornecedor)
            cell2.border = Rectangle.NO_BORDER
            tableProvider.addCell(cell2)

            cell1.phrase = Phrase("ENDEREÇO")
            cell1.border = Rectangle.NO_BORDER
            tableProvider.addCell(cell1)
            cell2.phrase = Phrase(it.endereco_fornecedor)
            cell2.border = Rectangle.NO_BORDER
            tableProvider.addCell(cell2)

            tableProvider.spacingAfter = 20F
            document.add(tableProvider)
            document.add(lineSeparator)
        }
        document.close()

        Toast.makeText(this, R.string.toast_pdf_success, Toast.LENGTH_SHORT).show()
        this.progressBarReport.visibility = View.GONE
    }










    @RequiresApi(Build.VERSION_CODES.Q)
    fun PDFAllProducts(v: View) {

        this.progressBarReport.visibility = View.VISIBLE

        val document = Document()
        //Criando um formatador para a data
        val s = SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
        //Diretorio de documentos do app
        val file = File(this.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), "AllProducts-${s.format(Date())}.pdf")
        PdfWriter.getInstance(document, FileOutputStream(file))
        PdfWriter.getInstance(document, FileOutputStream(file))
        document.open()

        //==========================================================================================
        //                                  Document Settings
        //==========================================================================================
        document.pageSize = PageSize.A4;
        document.addCreationDate();
        document.addCreator("Controle Fácil");
        //==========================================================================================

        val mColorAccent = BaseColor(0, 153, 204, 255)


        //==========================================================================================
        //Fazendo o cabeçalho do formulário ========================================================
        //==========================================================================================

        val title = Paragraph(5F, "Relatório de todos os Produtos", Font(Font.FontFamily.HELVETICA, 24.0f, Font.BOLD, BaseColor.BLACK))
        title.alignment = Element.ALIGN_CENTER
        title.spacingBefore = 50F
        document.add(title)


        val title2 = Paragraph(10F, "Todos os Produtos disponíveis em sua empresa", Font(Font.FontFamily.HELVETICA, 12.0f, Font.NORMAL, BaseColor.BLACK))
        title2.alignment     = Element.ALIGN_CENTER
        title2.spacingAfter  = 50F
        title2.spacingBefore = 50F
        document.add(title2)

        //==========================================================================================
        //==========================================================================================
        //==========================================================================================

        //==========================================================================================
        //Fazendo o cabeçalho do formulário ========================================================
        //==========================================================================================
        val cellName    = PdfPCell(Phrase("Nome Usuário:"))
        val cellEmpresa = PdfPCell(Phrase("Empresa"))
        val cellEmail   = PdfPCell(Phrase("Email"))
        val cellData    = PdfPCell(Phrase("Data"))
        val table = PdfPTable(4)
        table.paddingTop = 20F
        table.addCell(cellName)
        table.addCell(cellEmpresa)
        table.addCell(cellEmail)
        table.addCell(cellData)

        table.addCell(UserControl.getUserAuth().name)
        table.addCell(CompanyControl.getUserCompany().nome_empresa)
        table.addCell(UserControl.getUserAuth().email)
        table.addCell(LocalDate.now().toString())

        document.add(table)
        //==========================================================================================
        //==========================================================================================
        //==========================================================================================


        //==========================================================================================
        //Fazendo o cabeçalho do formulário ========================================================
        //==========================================================================================
        val subTitle = Paragraph("PRODUTOS")
        subTitle.alignment = Element.ALIGN_CENTER
        subTitle.font = Font(Font.FontFamily.COURIER)
        subTitle.spacingAfter  = 50F
        subTitle.spacingBefore = 50F
        document.add(subTitle)
        //==========================================================================================
        //==========================================================================================
        //==========================================================================================

        // LINE SEPARATOR
        val lineSeparator = LineSeparator()
        lineSeparator.lineColor = BaseColor.BLACK

        ProductControl.getAllProducts().forEach {

            document.add(lineSeparator)

            var attr  = PdfPCell()
            attr.border = Rectangle.NO_BORDER
            var value = PdfPCell()
            value.border = Rectangle.NO_BORDER
            var tableProduct = PdfPTable(2)
            tableProduct.addCell(attr)
            tableProduct.addCell(value)

            var cell1 = PdfPCell()
            var cell2 = PdfPCell()

            tableProduct.spacingBefore = 20F

            cell1.phrase = Phrase("NOME")
            cell1.border = Rectangle.NO_BORDER
            tableProduct.addCell(cell1)
            cell2.phrase = Phrase(it.nome_produto)
            cell2.border = Rectangle.NO_BORDER
            tableProduct.addCell(cell2)

            cell1.phrase = Phrase("FORNECEDOR")
            cell1.border = Rectangle.NO_BORDER
            tableProduct.addCell(cell1)
            cell2.phrase = Phrase(it.id_fornecedor.toString())
            cell2.border = Rectangle.NO_BORDER
            tableProduct.addCell(cell2)

            cell1.phrase = Phrase("PRECO")
            cell1.border = Rectangle.NO_BORDER
            tableProduct.addCell(cell1)
            cell2.phrase = Phrase(it.preco.toString())
            cell2.border = Rectangle.NO_BORDER
            tableProduct.addCell(cell2)

            cell1.phrase = Phrase("QUANTIDADE")
            cell1.border = Rectangle.NO_BORDER
            tableProduct.addCell(cell1)
            cell2.phrase = Phrase(it.qtd_disponivel.toString())
            cell2.border = Rectangle.NO_BORDER
            tableProduct.addCell(cell2)

            cell1.phrase = Phrase("STATUS")
            cell1.border = Rectangle.NO_BORDER
            tableProduct.addCell(cell1)
            cell2.phrase = Phrase(it.status.toString())
            cell2.border = Rectangle.NO_BORDER
            tableProduct.addCell(cell2)

            tableProduct.spacingAfter = 20F
            document.add(tableProduct)
            document.add(lineSeparator)
        }

        document.close()

        Toast.makeText(this, R.string.toast_pdf_success, Toast.LENGTH_SHORT).show()
        this.progressBarReport.visibility = View.GONE
    }



    //==============================================================================================
    //==============================================================================================
    //==============================================================================================
    fun onClickBackButton(v : View){
        super.onBackPressed()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================



    //==============================================================================================
    //==============================================================================================
    //==============================================================================================
    fun onLogoutButton(v : View){
        this.progressBarReport.visibility = View.VISIBLE
        JSONObjectRequestConcrete(
            this,
            Request.Method.POST,
            Routes.USER_LOGOUT.path,
            API.CURRENT.location,
            null,
            { _ ->
                this.progressBarReport.visibility = View.GONE
                moveTaskToBack(true);
                exitProcess(-1)
            },
            { _ ->
                this.progressBarReport.visibility = View.GONE
                Toast.makeText(this, R.string.toast_error_logout, Toast.LENGTH_SHORT).show()
            }).performRequestWithAuthentication()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================



}
