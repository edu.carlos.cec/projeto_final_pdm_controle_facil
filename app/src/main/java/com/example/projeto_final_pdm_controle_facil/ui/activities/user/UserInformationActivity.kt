package com.example.projeto_final_pdm_controle_facil.ui.activities.user

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.projeto_final_pdm_controle_facil.R
import com.example.projeto_final_pdm_controle_facil.api_reference.API
import com.example.projeto_final_pdm_controle_facil.api_reference.Routes
import com.example.projeto_final_pdm_controle_facil.global_data_control.AuthenticationControl
import com.example.projeto_final_pdm_controle_facil.global_data_control.UserControl
import com.example.projeto_final_pdm_controle_facil.http.JSONObjectRequestConcrete
import com.example.projeto_final_pdm_controle_facil.model.User
import com.example.projeto_final_pdm_controle_facil.model.data_classes.ApiResponse
import com.google.gson.Gson
import org.json.JSONObject
import java.util.HashMap
import kotlin.system.exitProcess

class UserInformationActivity : AppCompatActivity() {

    //==============================================================================================
    //==============================================================================================
    //==============================================================================================
    data class UserUpdate(
        val name              : String? = null,
        val email             : String? = null,
        val password          : String? = null,
        val updated_at        : String? = null
    )
    //==============================================================================================



    //==============================================================================================
    //                                          VIEWS
    //==============================================================================================
    private lateinit var etxtNameUser           : TextView
    private lateinit var etxtEmail              : TextView
    private lateinit var etxtPassword           : TextView
    private lateinit var backButton             : ImageView
    private lateinit var progressBarUpdateUser  : ProgressBar
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_information)

        //Pegar os valores das views do usuário ====================================================
        this.etxtNameUser          = findViewById(R.id.etxtNameUser)
        this.etxtEmail             = findViewById(R.id.etxtEmail)
        this.etxtPassword          = findViewById(R.id.etxtPassword)
        this.backButton            = findViewById(R.id.backButton)
        this.progressBarUpdateUser = findViewById(R.id.progressBarUpdateUser)
        //==========================================================================================

        this.etxtNameUser.text = UserControl.getUserAuth().name.toString()
        this.etxtEmail.text    = UserControl.getUserAuth().email.toString()
    }

    fun clickInsert(v : View){


        /*if( User.(this.etxtNameUser.text.toString(), this.etxtEmail.text.toString(), this.etxtPassword.text.toString()) ){

            //Fazer a requisicao de atualização ====================================================
            val queue = Volley.newRequestQueue(this)
            var url = "http://10.1.1.27:8000/api/usuarios/usuario/" + UserControl.getUserAuth().id_usuario
            val context = this
            //======================================================================================

            //Serializar os dados ==================================================================
            val gson = Gson()
            var user = UserUpdate(
                            name = this.etxtNameUser.text.toString(),
                            email = this.etxtEmail.text.toString(),
                            password = if( this.etxtPassword.text.toString().isEmpty() ) { null }
                                       else { this.etxtPassword.text.toString() },
                            updated_at = null
                            )
            var serializeUser = gson.toJson(user, UserUpdate::class.java)
            //======================================================================================



            //Fazer a requisicao ===================================================================
            var userUpdateRequest: JsonObjectRequest = object : JsonObjectRequest(
                Request.Method.PUT, url, JSONObject(serializeUser),
                Response.Listener<JSONObject?> {
                    response ->
                        //Request OK
                        //Atualizar o Singleton
                        UserControl.getUserAuth().name

                        //Recebendo a resposta da API
                        val gson = Gson()
                        val message = gson.fromJson<ApiResponse>(response.toString(), ApiResponse::class.java)
                        Toast.makeText(context, message.mensagem, Toast.LENGTH_SHORT).show()

                }, Response.ErrorListener {
                    Toast.makeText(context, "Não deu certo", Toast.LENGTH_SHORT).show()
                }) {
                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    var params: MutableMap<String, String> = super.getHeaders()
                    if (params.isEmpty()) {
                        params = HashMap()
                    }
                    params["Content-Type"] = "application/json"
                    params["Authorization"] = "Bearer ${AuthenticationControl.getAccessToken()}"
                    return params
                }
            }
            queue.add(userUpdateRequest)
        } else {
            //Retornar os problemas
            Toast.makeText(this, "Dados inválidos!", Toast.LENGTH_SHORT).show()
        }*/
        //======================================================================================



    }


    //==============================================================================================
    //                                      BACK BUTTON
    //==============================================================================================
    fun onClickBackButton(v : View){
        super.onBackPressed()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================



    //==============================================================================================
    //                                      LOGOUT BUTTON
    //==============================================================================================
    fun onLogoutButton(v : View){
        this.progressBarUpdateUser.visibility = View.VISIBLE
        JSONObjectRequestConcrete(
            this,
            Request.Method.POST,
            Routes.USER_LOGOUT.path,
            API.CURRENT.location,
            null,
            { _ ->
                this.progressBarUpdateUser.visibility = View.GONE
                moveTaskToBack(true);
                exitProcess(-1)
            },

            { _ ->
                this.progressBarUpdateUser.visibility = View.GONE
                Toast.makeText(this, R.string.toast_error_logout, Toast.LENGTH_SHORT).show()
            }).performRequestWithAuthentication()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================
}