package com.example.projeto_final_pdm_controle_facil.ui.activities.products

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.projeto_final_pdm_controle_facil.R
import com.example.projeto_final_pdm_controle_facil.api_reference.API
import com.example.projeto_final_pdm_controle_facil.api_reference.Routes
import com.example.projeto_final_pdm_controle_facil.global_data_control.AuthenticationControl
import com.example.projeto_final_pdm_controle_facil.global_data_control.CompanyControl
import com.example.projeto_final_pdm_controle_facil.global_data_control.ProductControl
import com.example.projeto_final_pdm_controle_facil.helper.GsonHelper
import com.example.projeto_final_pdm_controle_facil.http.JSONArrayRequestConcrete
import com.example.projeto_final_pdm_controle_facil.http.JSONObjectRequestConcrete
import com.example.projeto_final_pdm_controle_facil.model.Product
import com.example.projeto_final_pdm_controle_facil.model.Provider
import com.example.projeto_final_pdm_controle_facil.ui.lists.product.ProductListAdapter
import com.example.projeto_final_pdm_controle_facil.ui.spinners.ProvidersSpinnerAdapter
import com.google.gson.JsonObject
import org.json.JSONObject
import java.util.HashMap
import kotlin.system.exitProcess

class InsertProductActivity : AppCompatActivity() {

    //Activity Atributes ===========================================================================
    private lateinit var etxtProductName     : TextView
    private lateinit var spinnerProviders    : Spinner
    private lateinit var etxtProductPrice    : TextView
    private lateinit var etxtProductQuantity : TextView
    private lateinit var statusProduct       : SwitchCompat
    private lateinit var progressBarInsert   : ProgressBar
    //==============================================================================================

    private var idProvider          : Int?    = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_insert_product)

        //Inicializando os valores dos atributos ===================================================
        this.etxtProductName     = findViewById(R.id.etxtProviderName)
        this.etxtProductPrice    = findViewById(R.id.etxtProductPrice)
        this.etxtProductQuantity = findViewById(R.id.etxtProductQuantity)
        this.statusProduct       = findViewById(R.id.statusProduct)
        this.spinnerProviders    = findViewById(R.id.spinnerProviders)
        this.progressBarInsert   = findViewById(R.id.progressBarInsert)
        //==========================================================================================


        //==========================================================================================
        //                              FIND PROVIDERS TO PUT IN SPINNER
        //==========================================================================================
        val context = this
        JSONArrayRequestConcrete(this,
            Request.Method.GET,
            Routes.PROVIDERS.path,
            API.CURRENT.location,
            null,
            {
                response ->
                    val providers = GsonHelper().deserializeArray<Provider>(response.toString())
                    val arrayAdapter = ProvidersSpinnerAdapter(this, R.layout.provider_spinner, providers)
                    this.spinnerProviders.adapter = arrayAdapter
                    this.spinnerProviders.onItemSelectedListener = object : OnItemSelectedListener{
                        override fun onItemSelected(
                            av: AdapterView<*>?,
                            v:  View?,
                            p2: Int,
                            p3: Long
                        ) {
                            var idProvider : TextView? = v!!.findViewById(R.id.idProvider) as TextView?
                            context.idProvider = idProvider?.text.toString().toInt()
                        }

                        override fun onNothingSelected(p0: AdapterView<*>?) {
                            TODO("Not yet implemented")
                        }
                    }

            },
            {
                Toast.makeText(context, it.toString(), Toast.LENGTH_SHORT).show()
            }).performRequestWithAuthentication()
        //==========================================================================================

    }


    //==============================================================================================
    //                                          EVENTS
    //==============================================================================================
    fun onClickInsertProduct(v : View){

        val product = Product(
            id_produto     = null,
            id_empresa     = CompanyControl.getUserCompany().id_empresa,
            id_fornecedor  = this.idProvider,
            nome_produto   = if(this.etxtProductName.text.trim().isNotEmpty())
                            {
                                this.etxtProductName.text.toString()
                            } else {
                                null
                            },
            preco          = if( this.etxtProductPrice.text.isNotEmpty())
                            {
                                this.etxtProductPrice.text.toString().toFloat()
                            } else {
                                -1F
                            },
            qtd_disponivel = if( this.etxtProductQuantity.text.isNotEmpty())
                            {
                                this.etxtProductQuantity.text.toString().toFloat()
                            } else {
                                -1F
                            },
            status         = if( this.statusProduct.isChecked ) { 1 } else { 0 },
            created_at     = null,
            updated_at     = null

        )

        if( product.validate() ){

            JSONObjectRequestConcrete(
                this,
                Request.Method.POST,
                Routes.PRODUCTS_BY_COMPANY.path,
                API.CURRENT.location,
                JSONObject(GsonHelper().serialize(product)),
                { response ->

                        val product = GsonHelper().deserialize<Product>(response.toString()) as Product
                        val pos : Int = ProductControl.add(product)
                        resetFields()
                        val intent = Intent().apply{
                            putExtra("POSITION", pos)
                        }
                        setResult(RESULT_OK, intent)
                        finish()

                },
                {
                    Toast.makeText(baseContext, it.toString(), Toast.LENGTH_SHORT).show()
                }).performRequestWithAuthentication()

        } else {
            Toast.makeText(this, R.string.toast_insert_product_error, Toast.LENGTH_LONG).show()
        }
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    //==============================================================================================
    //                                      RESET FIELDS
    //==============================================================================================
    fun resetFields(){
        this.etxtProductName.text = ""
        this.etxtProductPrice.text = ""
        this.etxtProductQuantity.text = ""
        this.statusProduct.isChecked = false
        this.spinnerProviders
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    //==============================================================================================
    //                                          BACK BUTTON
    //==============================================================================================
    fun onClickBackButton(v : View){
        super.onBackPressed()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    //==============================================================================================
    //                                          LOGOUT BUTTON
    //==============================================================================================
    fun onLogoutButton(v : View){
        this.progressBarInsert.visibility = View.VISIBLE
        JSONObjectRequestConcrete(
            this,
            Request.Method.POST,
            Routes.USER_LOGOUT.path,
            API.CURRENT.location,
            null,
            { _ ->
                this.progressBarInsert.visibility = View.GONE
                moveTaskToBack(true);
                exitProcess(-1)
            },

            { _ ->
                this.progressBarInsert.visibility = View.GONE
                Toast.makeText(this, R.string.toast_error_logout, Toast.LENGTH_SHORT).show()
            }).performRequestWithAuthentication()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================
}

