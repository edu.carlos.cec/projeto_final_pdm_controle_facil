package com.example.projeto_final_pdm_controle_facil.ui.lists.finance

import android.view.View
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.projeto_final_pdm_controle_facil.R
import com.example.projeto_final_pdm_controle_facil.model.SellHistory
import com.example.projeto_final_pdm_controle_facil.model.Product


//O itemView é como toda a View
//O construtor da ViewHolder deve sempre definir seu itemView para a superclasse
//Recebemos o adapter para ter acesso ao listener
class FinanceViewHolder(

    itemView : View,
    private val adapter: FinanceListAdapter

) : RecyclerView.ViewHolder(itemView){

    //Aqui temos todas as views que precisa ser guardadas
    private val id_sell          : TextView  = itemView.findViewById(R.id.id_sell)
    private val id_sell_product  : TextView  = itemView.findViewById(R.id.id_sell_product)
    private val sell_quantity    : TextView  = itemView.findViewById(R.id.sell_quantity)
    private val sell_total       : TextView  = itemView.findViewById(R.id.sell_total)

    private lateinit var finance : SellHistory //Mantem a informacao da Task


    //Vincula a task com as Views
    //Aqui acontece a reciclagem da ViewHolder
    //Será chamado toda vez que houver necessidade de reciclar os dados da itemView
    fun bind(finance: SellHistory, position : Int) {
        this.finance = finance
        this.id_sell.text         = finance.id_venda.toString()
        this.id_sell_product.text = finance.id_produto.toString()
        this.sell_quantity.text   = finance.qtd_venda.toString()
        this.sell_total.text      = finance.preco_total.toString()
    }


}