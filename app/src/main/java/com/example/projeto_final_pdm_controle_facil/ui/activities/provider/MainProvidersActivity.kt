package com.example.projeto_final_pdm_controle_facil.ui.activities.provider

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.example.projeto_final_pdm_controle_facil.R
import com.example.projeto_final_pdm_controle_facil.api_reference.API
import com.example.projeto_final_pdm_controle_facil.api_reference.Routes
import com.example.projeto_final_pdm_controle_facil.global_data_control.CompanyControl
import com.example.projeto_final_pdm_controle_facil.global_data_control.ProductControl
import com.example.projeto_final_pdm_controle_facil.global_data_control.ProviderControl
import com.example.projeto_final_pdm_controle_facil.global_data_control.UserControl
import com.example.projeto_final_pdm_controle_facil.helper.GsonHelper
import com.example.projeto_final_pdm_controle_facil.http.JSONArrayRequestConcrete
import com.example.projeto_final_pdm_controle_facil.http.JSONObjectRequestConcrete
import com.example.projeto_final_pdm_controle_facil.model.Product
import com.example.projeto_final_pdm_controle_facil.model.Provider
import com.example.projeto_final_pdm_controle_facil.ui.activities.products.InsertProductActivity
import com.example.projeto_final_pdm_controle_facil.ui.activities.products.UpdateProductActivity
import com.example.projeto_final_pdm_controle_facil.ui.alertBuilder.DeleteDialog
import com.example.projeto_final_pdm_controle_facil.ui.lists.product.ProductListAdapter
import com.example.projeto_final_pdm_controle_facil.ui.lists.provider.ProviderListAdapter
import com.example.projeto_final_pdm_controle_facil.ui.lists.provider.ProviderViewHolder
import kotlin.system.exitProcess

class MainProvidersActivity : AppCompatActivity() {


    //==============================================================================================
    //                                          VIEWS
    //==============================================================================================
    private lateinit var rvProviderList          : RecyclerView
    private lateinit var userName                : TextView
    private lateinit var companyName             : TextView
    private lateinit var search_provider         : EditText
    private lateinit var resultLauncher          : ActivityResultLauncher<Intent>
    private lateinit var resultLauncherUpdate    : ActivityResultLauncher<Intent>
    private lateinit var progressBarMainProvider : ProgressBar
    //==============================================================================================


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_providers)

        //==========================================================================================
        this.rvProviderList  = findViewById(R.id.rvProviderList)
        this.userName        = findViewById(R.id.userName)
        this.companyName     = findViewById(R.id.companyName)
        this.search_provider = findViewById(R.id.search_provider)
        this.progressBarMainProvider = findViewById(R.id.progressBarMainProvider)
        //==========================================================================================

        //==========================================================================================
        this.userName.text        = UserControl.getUserAuth().name
        this.companyName.text     = CompanyControl.getUserCompany().nome_empresa
        //==========================================================================================


        //==========================================================================================
        this.search_provider.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s!!.isNotEmpty()){
                    (rvProviderList.adapter as ProviderListAdapter).filterProvider(true, s.toString())
                } else {
                    (rvProviderList.adapter as ProviderListAdapter).filterProvider(false, s.toString())
                }

            }
        })
        //==========================================================================================




        //==========================================================================================
        val context = this
        JSONArrayRequestConcrete(
            this,
            Request.Method.GET,
            Routes.PROVIDERS.path,
            API.CURRENT.location,
            null,
            {
                response ->
                    val providers = GsonHelper().deserializeArray<Provider>(response.toString())
                    ProviderControl.addAllProviders(providers)
                    this.rvProviderList.layoutManager = LinearLayoutManager(context)
                    this.rvProviderList.setHasFixedSize(true)
                    this.rvProviderList.adapter = ProviderListAdapter(ProviderControl.getAllProviders())
                        .setOnClickDeleteProviderListener{

                            object : DeleteDialog(this){
                                override fun delete() {
                                    JSONObjectRequestConcrete(
                                        baseContext,
                                        Request.Method.DELETE,
                                        Routes.SPECIFIED_PROVIDER.path + it.id_fornecedor,
                                        API.CURRENT.location,
                                        null,
                                        { _ ->
                                                Toast.makeText(context, R.string.toast_delete_success, Toast.LENGTH_SHORT).show()
                                                val pos = ProviderControl.delete(it)
                                                rvProviderList.adapter?.notifyItemRemoved(pos)
                                        },
                                        {
                                            Toast.makeText(context, R.string.toast_delete_error, Toast.LENGTH_SHORT).show()
                                        }
                                    ).performRequestWithAuthentication()
                                }
                            }

                        }
                        .setOnClickUpdateProviderListener{
                            // UPDATE ==============================================================
                            val requesProviderUpdate = Intent(this, UpdateProviderActivity::class.java).apply {
                                putExtra("PROVIDER", it)
                            }
                            this.resultLauncherUpdate.launch(requesProviderUpdate)
                            //======================================================================
                        }

            },
            {
                Toast.makeText(context, it.toString(), Toast.LENGTH_SHORT).show()
            }).performRequestWithAuthentication()

        //==========================================================================================


        buildResultLauncher()
        buildResultLauncherUpdate()
    }





    //==============================================================================================
    //                                      CRIAÇÃO
    // =============================================================================================

    fun onClickInsertProvider(v : View){
        val requestProvider = Intent(this, InsertProviderActivity::class.java)
        this.resultLauncher.launch(requestProvider)
    }

    private fun buildResultLauncher(){
        val contract = ActivityResultContracts.StartActivityForResult()
        this.resultLauncher = registerForActivityResult(contract){
                result ->
                    if(result.resultCode == RESULT_OK && result.data != null){
                        val pos = result.data!!.getIntExtra("POSITION", -1)
                        rvProviderList.adapter?.notifyItemInserted(pos)
                    }
        }
    }

    //==============================================================================================
    //==============================================================================================
    //==============================================================================================




    //==============================================================================================
    //                                      UPDATE
    // =============================================================================================
    private fun buildResultLauncherUpdate(){
        val contract = ActivityResultContracts.StartActivityForResult()
        this.resultLauncherUpdate = registerForActivityResult(contract){
                result ->
            if(result.resultCode == RESULT_OK && result.data != null){
                val prov = result.data!!.getParcelableExtra<Provider>("FORNECEDOR_RESPOSTA")
                val pos = ProviderControl.update(prov!!)
                rvProviderList.adapter?.notifyItemChanged(pos)
            }
        }
    }

    //==============================================================================================
    //==============================================================================================
    //==============================================================================================



    //==============================================================================================
    //                                     BACK BUTTON
    //==============================================================================================
    fun onClickBackButton(v : View){
        super.onBackPressed()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    //==============================================================================================
    //                                     LOGOUT BUTTON
    //==============================================================================================
    fun onLogoutButton(v : View){

        this.progressBarMainProvider.visibility = View.VISIBLE

        JSONObjectRequestConcrete(
            this,
            Request.Method.POST,
            Routes.USER_LOGOUT.path,
            API.CURRENT.location,
            null,
            { _ ->
                this.progressBarMainProvider.visibility = View.GONE
                moveTaskToBack(true);
                exitProcess(-1)
            },

            { _ ->
                this.progressBarMainProvider.visibility = View.GONE
                Toast.makeText(this, R.string.toast_error_logout, Toast.LENGTH_SHORT).show()
            }).performRequestWithAuthentication()

    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================



}