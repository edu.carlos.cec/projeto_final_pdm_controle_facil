package com.example.projeto_final_pdm_controle_facil.ui.alertBuilder

import android.app.AlertDialog
import android.content.Context
import com.example.projeto_final_pdm_controle_facil.R

abstract class DeleteDialog(val context: Context) {

    init {
        AlertDialog.Builder(context)
            .setTitle(R.string.toast_delete_title)
            .setMessage(R.string.toast_delete_message)
            .setPositiveButton(R.string.toast_delete_button) { _, _ ->
                this.delete()
            }
            .setNegativeButton(R.string.toast_cancel){ _, _ ->

            }
            .create().show()
    }

    abstract fun delete()
}