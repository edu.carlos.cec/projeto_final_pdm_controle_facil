package com.example.projeto_final_pdm_controle_facil.ui.lists.provider

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.projeto_final_pdm_controle_facil.R
import com.example.projeto_final_pdm_controle_facil.model.Provider

class ProviderViewHolder(
    itemView : View,
    private val adapter: ProviderListAdapter
) : RecyclerView.ViewHolder(itemView){

    //Aqui temos todas as views que precisa ser guardadas ==========================================
    private val idProvider          : TextView  = itemView.findViewById(R.id.id_sell)
    private val nameProvider        : TextView  = itemView.findViewById(R.id.id_sell_product)
    private val addressProvider     : TextView  = itemView.findViewById(R.id.sell_quantity)
    private val dateInit            : TextView  = itemView.findViewById(R.id.sell_total)
    private val dateFinal           : TextView  = itemView.findViewById(R.id.date_final_provider)
    private val editProvider        : ImageView = itemView.findViewById(R.id.edit_provider)
    private val deleteProvider     : ImageView = itemView.findViewById(R.id.delete_provider)
    private lateinit var provider   : Provider //Mantem a informacao da Task
    //==============================================================================================


    fun bind(provider: Provider, position : Int) {
        this.provider = provider
        this.idProvider.text       = provider.id_fornecedor.toString()
        this.nameProvider.text     = provider.nome_fornecedor
        this.addressProvider.text  = provider.endereco_fornecedor
        this.dateInit.text         = provider.created_at
        this.dateFinal.text        = provider.updated_at

        this.editProvider.setOnClickListener{
            onClickUpdate()
        }

        this.deleteProvider.setOnClickListener{
            onClickDelete()
        }
    }


    private fun onClickDelete(){
        this.adapter.getOnClickDeleteProviderListener()?.onClick(this.provider) //Se o ONCLICK for diferente de null
    }

    private fun onClickUpdate(){
        this.adapter.getOnClickUpdateProviderListener()?.onClick(this.provider) //Se o ONCLICK for diferente de null
    }

}