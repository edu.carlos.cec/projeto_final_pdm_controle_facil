package com.example.projeto_final_pdm_controle_facil.ui.lists.provider

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.projeto_final_pdm_controle_facil.R
import com.example.projeto_final_pdm_controle_facil.model.Product
import com.example.projeto_final_pdm_controle_facil.model.Provider
import com.example.projeto_final_pdm_controle_facil.ui.lists.product.ProductListAdapter

class ProviderListAdapter(
    private var providerList : ArrayList<Provider>
) : RecyclerView.Adapter<ProviderViewHolder>(){

    private var providerListInitial = this.providerList
    private var listenerDelete      : OnClickDeleteProvider? = null
    private var listenerUpdate      : OnClickUpdateProvider? = null


    //==============================================================================================
    //          Criando uma interface para receber a implementação do evento de click
    //==============================================================================================
    fun interface OnClickDeleteProvider{
        fun onClick(provider : Provider)
    }

    fun interface OnClickUpdateProvider{
        fun onClick(provider : Provider)
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProviderViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.default_provider_layout, parent, false)
        return ProviderViewHolder(itemView, this)
    }

    override fun onBindViewHolder(holder: ProviderViewHolder, position: Int) {
        holder.bind(this.providerList[position], position)
    }

    override fun getItemCount(): Int {
        return providerList.size
    }


    //==============================================================================================
    //==============================================================================================
    //==============================================================================================
    fun setOnClickDeleteProviderListener(listener : OnClickDeleteProvider) : ProviderListAdapter {
        this.listenerDelete = listener
        return this
    }
    fun getOnClickDeleteProviderListener() : OnClickDeleteProvider? {
        return this.listenerDelete
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================




    //==============================================================================================
    //==============================================================================================
    //==============================================================================================
    fun setOnClickUpdateProviderListener(listener : OnClickUpdateProvider) : ProviderListAdapter {
        this.listenerUpdate = listener
        return this
    }
    fun getOnClickUpdateProviderListener() : OnClickUpdateProvider? {
        return this.listenerUpdate
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================



    fun filterProvider(filter : Boolean, pattern: String){
        if(filter){
            this.providerList = this.providerList.filter { it.nome_fornecedor?.lowercase()?.contains(pattern.lowercase()) as Boolean } as ArrayList<Provider>
            if(this.providerList.isEmpty()){
                this.providerList = this.providerListInitial
            }
        } else {
            this.providerList = this.providerListInitial
        }
        notifyDataSetChanged() //Faça a atualização da lista -> como tivesse apagando e colocando tudo outra vez
    }

}