package com.example.projeto_final_pdm_controle_facil.ui.alertBuilder

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import com.example.projeto_final_pdm_controle_facil.R

abstract class EditQuantityDialog(val context : Context) {

    //==============================================================================================
    //                                        ATRIBUTES
    //==============================================================================================
    private val quantitySum   : TextView
    private val addProduct    : ImageView
    private val removeProduct : ImageView
    private var quantity      : Int = 0
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================



    init{
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.edit_product_quantity, null)
        this.addProduct    = layout.findViewById(R.id.addProduct)
        this.removeProduct = layout.findViewById(R.id.removeProduct)
        this.quantitySum   = layout.findViewById(R.id.quantitySum)
        this.quantitySum.text = quantity.toString()

        this.addProduct.setOnClickListener{
            this.quantitySum.text = (++this.quantity).toString()
        }

        this.removeProduct.setOnClickListener{
            if(this.quantity != 0)
                this.quantitySum.text = (--this.quantity).toString()
        }

        AlertDialog.Builder(context)
            .setTitle(R.string.toast_edit_quantity)
            .setView(layout)
            .setPositiveButton(R.string.toast_edit) { _, _ ->
                this.onUpdate(this.quantity)
            }
            .setNegativeButton(R.string.toast_cancel){_,_->

            }
            .create().show()
    }

    abstract fun onUpdate(quantity : Int)


}