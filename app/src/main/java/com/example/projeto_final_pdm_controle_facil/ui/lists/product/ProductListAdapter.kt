package com.example.projeto_final_pdm_controle_facil.ui.lists.product

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.projeto_final_pdm_controle_facil.R
import com.example.projeto_final_pdm_controle_facil.model.Product
import java.util.regex.Pattern

//Criação do RecyclerView de produto
class ProductListAdapter(

    private var productList : ArrayList<Product>

) : RecyclerView.Adapter<ProductViewHolder>(){

    private var productListInitial : ArrayList<Product> = this.productList
    private var listenerDelete : OnClickDeleteProduct? = null
    private var listenerUpdate : OnClickUpdateProduct? = null
    private var listenerAddToHistory : OnClickAddToHistory? = null

    //==============================================================================================
    //          Criando uma interface para receber a implementação do evento de click
    //==============================================================================================
    
    fun interface OnClickDeleteProduct {
        fun onClick(product : Product?)
    }

    fun interface OnClickUpdateProduct {
        fun onClick(product : Product?)
    }

    fun interface OnClickAddToHistory {
        fun onClick(product : Product?)
    }

    //==============================================================================================
    //==============================================================================================
    //==============================================================================================



    //==============================================================================================
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.default_product_layout, parent, false)
        return ProductViewHolder(itemView, this)
    }

    //Recicla os valores que estao na itemView, usando o ViewHolder
    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(this.productList[position], position)
    }

    override fun getItemCount(): Int {
        return this.productList.size
    }
    //==============================================================================================



    //==============================================================================================
    //==============================================================================================
    //==============================================================================================
    fun setOnClickDeleteProductListener(listener : OnClickDeleteProduct) : ProductListAdapter {
        this.listenerDelete = listener
        return this
    }
    fun getOnClickDeleteListener() : OnClickDeleteProduct? {
        return this.listenerDelete
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================




    //==============================================================================================
    //==============================================================================================
    //==============================================================================================
    fun setOnClickUpdateProductListener(listener : OnClickUpdateProduct) : ProductListAdapter {
        this.listenerUpdate = listener
        return this
    }
    fun getOnClickUpdateListener() : OnClickUpdateProduct? {
        return this.listenerUpdate
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    //==============================================================================================
    //==============================================================================================
    //==============================================================================================
    fun setOnClickAddToHistory(listener : OnClickAddToHistory) : ProductListAdapter {
        this.listenerAddToHistory = listener
        return this
    }
    fun getOnClickAddToHistory() : OnClickAddToHistory? {
        return this.listenerAddToHistory
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    fun filterProduct(filter : Boolean, pattern: String){
        if(filter){
            this.productList = this.productList.filter { it.nome_produto?.lowercase()?.contains(pattern.lowercase()) as Boolean } as ArrayList<Product>
            if(this.productList.isEmpty()){
                this.productList = this.productListInitial
            }
        } else {
            this.productList = this.productListInitial
        }
        notifyDataSetChanged() //Faça a atualização da lista -> como tivesse apagando e colocando tudo outra vez
    }




}
