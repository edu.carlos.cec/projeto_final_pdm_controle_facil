package com.example.projeto_final_pdm_controle_facil.ui.activities.products

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.widget.SwitchCompat
import com.android.volley.Request
import com.example.projeto_final_pdm_controle_facil.R
import com.example.projeto_final_pdm_controle_facil.api_reference.API
import com.example.projeto_final_pdm_controle_facil.api_reference.Routes
import com.example.projeto_final_pdm_controle_facil.global_data_control.CompanyControl
import com.example.projeto_final_pdm_controle_facil.global_data_control.ProductControl
import com.example.projeto_final_pdm_controle_facil.helper.GsonHelper
import com.example.projeto_final_pdm_controle_facil.http.JSONArrayRequestConcrete
import com.example.projeto_final_pdm_controle_facil.http.JSONObjectRequestConcrete
import com.example.projeto_final_pdm_controle_facil.model.Product
import com.example.projeto_final_pdm_controle_facil.model.Provider
import com.example.projeto_final_pdm_controle_facil.ui.spinners.ProvidersSpinnerAdapter
import org.json.JSONObject
import kotlin.system.exitProcess

class UpdateProductActivity : AppCompatActivity() {

    private var idProvider : Int? = 0

    // VIEWS DAS ACITIVITIES =======================================================================
    private lateinit var etxtProductName          : TextView
    private lateinit var spinnerProviders         : Spinner
    private lateinit var etxtProductPrice         : TextView
    private lateinit var etxtProductQuantity      : TextView
    private lateinit var statusProduct            : SwitchCompat
    private lateinit var product                  : Product
    private lateinit var progressBarUpdateProduct : ProgressBar
    //==============================================================================================



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_product)

        // FIND VIEWS ==============================================================================
        this.etxtProductName           = findViewById(R.id.etxtProviderName)
        this.spinnerProviders          = findViewById(R.id.spinnerProviders)
        this.etxtProductPrice          = findViewById(R.id.etxtProductPrice)
        this.etxtProductQuantity       = findViewById(R.id.etxtProductQuantity)
        this.statusProduct             = findViewById(R.id.statusProduct)
        this.progressBarUpdateProduct  = findViewById(R.id.progressBarUpdateProduct)
        //==========================================================================================


        val context = this
        JSONArrayRequestConcrete(
            this,
            Request.Method.GET,
            Routes.PROVIDERS.path,
            API.CURRENT.location,
            null,
            { response ->
                val providers = GsonHelper().deserializeArray<Provider>(response.toString())
                val arrayAdapter = ProvidersSpinnerAdapter(this, R.layout.provider_spinner, providers)
                this.spinnerProviders.adapter = arrayAdapter
                this.spinnerProviders.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        av: AdapterView<*>?,
                        v: View?,
                        p2: Int,
                        p3: Long
                    ) {
                        var idProvider : TextView? = v!!.findViewById(R.id.idProvider) as TextView?
                        context.idProvider = idProvider?.text.toString().toInt()
                    }

                    override fun onNothingSelected(p0: AdapterView<*>?) {
                        TODO("Not yet implemented")
                    }
                }
            },
            {
                Toast.makeText(context, it.toString(), Toast.LENGTH_SHORT).show()
            }).performRequestWithAuthentication()


        //==========================================================================================
        //                  Recebendo os dados de produto para update
        this.product = intent.getParcelableExtra<Product>("PRODUTO")!!
        //==========================================================================================

        //Setando os valores dos inputs para os dados do Singleton =================================
        this.etxtProductName.text     = this.product.nome_produto
        this.etxtProductPrice.text    = this.product.preco.toString()
        this.etxtProductQuantity.text = this.product.qtd_disponivel.toString()
        this.statusProduct.isChecked  = this.product.status == 1
        //==========================================================================================

    }


    //==============================================================================================
    val context = this
    fun updateProduct(v : View){

        //Settando os valores no object  ===========================================================
        this.product = Product(
            id_produto     = this.product.id_produto,
            id_empresa     = CompanyControl.getUserCompany().id_empresa,
            id_fornecedor  = this.idProvider,
            nome_produto   = if(this.etxtProductName.text.trim().isNotEmpty())
                            {
                                this.etxtProductName.text.toString()
                            } else {
                                null
                            },
            preco          = if( this.etxtProductPrice.text.isNotEmpty())
                            {
                                this.etxtProductPrice.text.toString().toFloat()
                            } else {
                                -1F
                            },
            qtd_disponivel = if( this.etxtProductQuantity.text.isNotEmpty())
                            {
                                this.etxtProductQuantity.text.toString().toFloat()
                            } else {
                                -1F
                            },
            status         = if( this.statusProduct.isChecked ) { 1 } else { 0 },
            created_at     = null,
            updated_at     = null

        )
        if(this.product.validate()) {
            val productJson: String = GsonHelper().serialize(product)
            JSONObjectRequestConcrete(
                this,
                Request.Method.PUT,
                Routes.SPECIFIED_PRODUCT.path + product.id_produto,
                API.CURRENT.location,
                JSONObject(productJson),
                { _ ->
                    //==========================================================================
                    val response = Intent().apply {
                        putExtra("PRODUTO_RESPOSTA", product)
                    }
                    setResult(RESULT_OK, response)
                    finish()
                    //==========================================================================

                },
                {
                    Toast.makeText(context, it.toString(), Toast.LENGTH_SHORT).show()
                }).performRequestWithAuthentication()
            //==========================================================================================
        }

    }
    //==============================================================================================



    //==============================================================================================
    //                                           BUTTON BACK
    //==============================================================================================
    fun onClickBackButton(v : View){
        super.onBackPressed()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================


    //==============================================================================================
    //                                           BUTTON LOGOUT
    //==============================================================================================
    fun onLogoutButton(v : View){
        this.progressBarUpdateProduct.visibility = View.VISIBLE
        JSONObjectRequestConcrete(
            this,
            Request.Method.POST,
            Routes.USER_LOGOUT.path,
            API.CURRENT.location,
            null,
            { _ ->
                this.progressBarUpdateProduct.visibility = View.GONE
                moveTaskToBack(true);
                exitProcess(-1)
            },

            { _ ->
                this.progressBarUpdateProduct.visibility = View.GONE
                Toast.makeText(this, R.string.toast_error_logout, Toast.LENGTH_SHORT).show()
            }).performRequestWithAuthentication()
    }
    //==============================================================================================
    //==============================================================================================
    //==============================================================================================

}
